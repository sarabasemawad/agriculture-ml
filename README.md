# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Data sets ##
### Daily ###
+ This folder contains an excel sheet [daily data set](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Daily/daily_dataset.csv) which has the values of weather parameters, ancillary parameters, and evapotranspiration at a daily time stamp.
This is the manual data set which consits of 10,911 rows. The data is further divided into training and testing data sets:
    * [Train](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Daily/manual_train.csv)
    * [Test](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Daily/manual_test.csv)
### Seasonality ###
+ This folder contains several data sets. We have obtained these data sets by clustering by air temperature (TA) with Kmeans of K=3 resulting in three sheets where each pertains to one season:
    * [Summer](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Seasonality/kmeans_TA_Cluster_0.csv)
    * [Winter](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Seasonality/kmeans_TA_Cluster_1.csv)
    * [Spring](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Seasonality/kmeans_TA_Cluster_2.csv) 

The data is further divided into [training](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Seasonality/train/) and [testing](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Seasonality/test/) data sets.
### Climate Clustering ###
+ This folder contains several data sets. We have ontained these data sets by subsetting on all the climates resulting in five sheets where each pertains to one climate. The data is further divided into training and testing as follows:
    * [Climate Cfa](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Climate%20Clustering/train/cfa_manual_train.csv)
    * [Climate Csa](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Climate%20Clustering/train/csa_manual_train.csv)
    * [Climate Csb](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Climate%20Clustering/train/csb_manual_train.csv)
    * [Climate Cwa](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Climate%20Clustering/train/cwa_manual_train.csv)
    * [Climate Dsa](https://bitbucket.org/sarabasemawad/agriculture-ml/src/master/Data%20sets/Climate%20Clustering/train/dsa_manual_train.csv)
## Modules ##
+ This folder represents two main modules:
    * [Point-wise Prediction:]() This module contains two models: [Meta-Learning]() and [Multi-Layer Perceptron]().
    * [Probabilistic Prediction:]() This module contains two models: [MC Dropout]() and [Deep Ensemble]().
+ Each model type consits of three sub-folders:
    * Code: This folder contains all the logic for setting up hyper-parameters, training, and evaluating the model.
    * Experiments: This folder contains all the output scores and visuals for each data set experiment i.e (Daily, Seasonality, and Climate Clustering). Each experiment is performed on all the feature selection scenarios which are divided as follows:
        - No FS: It revolves around having a model with all the input features as in without applying any feature selection method. The columns for this scenario are Site Encoded, Month Encoded, Vegetation Encoded, TA + 5 Lags, NDVI + 5 lags, LST + 5 lags, Albedo + 5 lags, WS+ 5 lags, and RH + 5 lags.
        - FS1: It revolves around having a model that is economically inexpensive with the least training time and least demanding in terms of input features. The columns for this scenario are Site Encoded, Month Encoded, Vegetation Encoded, TA, and 5 of its lags.
        - FS2: It revolves around having a model that is fed a certain number of features less than the total number of features. The columns for this scenario are Month Encoded, Site Encoded, TA + 5 Lags, NDVI + 2 lags, LST + 5 lags, Albedo + 2 lags, WS+ 2 lags, and RH + 3 lags.
        - FS3: It revolves around having a model that is fed columns that have shown to be the top contributing columns to our target variable according to SHAP. The columns for this scenario are Ta + 5 Lags, LST + 5 lags, and RH + 3 lags.
    * Papers: This folder contains the literature review papers we have used in our study.
## Analysis&Interpretibility ##
+ This folder contains several analysis and interpretibility tools:
    * [EDA:]() This folder consists of the exploratory data analysis performed on our data set.
    * [Feature Selection:]() This folder consits of the feature selection methods applied on our data set.
    * [LIME:]() This folder consists of the LIME study performed on our data set.
    * [SHAP:]() This folder consists of the SHAP study performed on our data set.
    * [Time-series Study:]() This folder consists of the time-series study performed on our data set.