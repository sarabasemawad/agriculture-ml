import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

output_path = "/Users/saraawad/Workspace/Agriculture-ML/Analysis&Interpretibility/Plotting/output/"

def get_unique_sites(paths):
    df = pd.read_csv("/Users/saraawad/Workspace/Agriculture-ML/Analysis&Interpretibility/Plotting/Code/data sets/" + paths[0] + ".csv", delimiter=',')
    unique_sites = df['Site Id'].unique()
    return unique_sites

def get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows=0, site_id=''):
    df_list = {}
    for i in range(len(paths)):
        path = paths[i]
        model_type = types[i]
        df = pd.read_csv("/Users/saraawad/Workspace/Agriculture-ML/Analysis&Interpretibility/Plotting/Code/data sets/" + path + ".csv", delimiter=',')
        df = df[columns]
        # df['Date'] = pd.to_datetime(df.Date)
        # df.sort_values(by=['Date'], inplace=True, ascending=True)
        # df.reset_index(drop=True)
        df['Model'] = model_type
        if site_id != '':
            df = df[df['Site Id'] == site_id]
        if num_rows != 0:
            df = df[:num_rows]
        df = df[['Model', uncertainty_column]]
        print('df size', df.shape[0])
        df_list[model_type] = df
    return df_list

def plot_uncertainty(df_list, uncertainty_column, selected_site_id=''):
    fig, ax = plt.subplots()
    print('df keys are', df_list.keys())
    for df in df_list.keys():
        print('df is ', df)
        df_list[df].plot(ax=ax)
    plt.legend(types, loc='upper left')
    ax.set_xlabel('Data points')
    ax.set_ylabel(uncertainty_column + '(mm2)')
    if selected_site_id != '':
        plt.suptitle(uncertainty_column + ' for Site ' + selected_site_id)
        plt.savefig(output_path + uncertainty_column + '_' + selected_site_id)
    else:
        plt.suptitle(uncertainty_column)
        plt.savefig(output_path + uncertainty_column)

def plot_target_variance(path, model_type, isMDN=False):
    df = pd.read_csv(path + ".csv", delimiter=',')
    columns = ['Site Id', 'Date', 'Aleatoric Uncertainty',
        'Epistemic Uncertainty', 'Total Uncertainty', 'LE_bowen_corr(mm)',
            'LE_bowen_corr(mm)', 'LE_bowen_corr(mm)_predicted']
    df = df[columns]
    df = df[:100]
    actual_column = df['LE_bowen_corr(mm)']
    print("is MDN", isMDN)
    # if not isMDN:
    #     predicted_column = df['LE_bowen_corr(mm)_predicted']
    # else:
    predicted_column = df['LE_bowen_corr(mm)_predicted']
    uncertainty_column = df['Total Uncertainty']
    plt.plot(list(range(1, len(actual_column) + 1)), actual_column, color='b', label='actual')
    # plt.plot(list(range(1, len(actual_column) + 1)), predicted_column, color='r', label='predicted')
    plt.plot(list(range(1, len(actual_column) + 1)), uncertainty_column, color='g', label='uncertainty')
    plt.suptitle(model_type + " Target and Total Variance")
    # plt.savefig(self.output_path + file_name + '_actual_vs_predicted')
    plt.show()
    plt.close()

## Start plotting here
paths = ['nofs', 'fs1', 'fs2', 'fs3']
types = ['DeepEnsemble1', 'DeepEnsemble2', 'DeepEnsemble3', 'DeepEnsemble4']
columns = ['Site Id', 'Date', 'Aleatoric Uncertainty',
       'Epistemic Uncertainty', 'Total Uncertainty']
num_rows = 100
# uncertainty_columns = ['Epistemic Uncertainty']
uncertainty_columns = ['Aleatoric Uncertainty', 'Epistemic Uncertainty', 'Total Uncertainty']
unique_sites = get_unique_sites(paths)
print(unique_sites)

## Plot all data for each uncertainty type and each model
# site_id = ''
# for uncertainty_column in uncertainty_columns:
#     df_list = get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows, site_id)
#     plot_uncertainty(df_list, uncertainty_column, site_id)

# Plot all data for each uncertainty model
for uncertainty_column in uncertainty_columns:
    df_list = get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows)
    plot_uncertainty(df_list, uncertainty_column)

# Plot for each site each uncertainty type per model for 100 rows
# for site_id in unique_sites:
#     for uncertainty_column in uncertainty_columns:
#         df_list = get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows, site_id)
#         plot_uncertainty(df_list, uncertainty_column, site_id)