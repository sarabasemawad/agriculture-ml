import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def get_unique_sites(paths):
    df = pd.read_csv(paths[0] + ".csv", delimiter=',')
    unique_sites = df['Site Id'].unique()
    return unique_sites

def get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows=100, site_id=''):
    df_list = {}
    for i in range(len(paths)):
        path = paths[i]
        model_type = types[i]
        file_path = "/Users/saraawad/Workspace/Agriculture-ML/Analysis&Interpretibility/Plotting/Code/data sets/" + path + ".csv"
        df = pd.read_csv(file_path, delimiter=',')
        df = df[columns]
        # df['Date'] = pd.to_datetime(df.Date)
        # df.sort_values(by=['Date'], inplace=True, ascending=True)
        # df.reset_index(drop=True)
        df['Model'] = model_type
        if site_id != '':
            df = df[df['Site Id'] == site_id]
        df = df[:num_rows]
        # df.drop(['Site Id', 'Date', 'Aleatoric Uncertainty',
        # 'Epistemic Uncertainty'], axis=1, inplace=True)
        df = df[['Model', uncertainty_column]]
        print('df size', df.shape[0])
        df_list[model_type] = df
    return df_list

def plot_uncertainty(df_list, uncertainty_column, selected_site_id):
    fig, ax = plt.subplots()
    for df in df_list.keys():
        df_list[df].plot(ax=ax)
    plt.legend(types, loc='upper left')
    ax.set_xlabel('Data points')
    ax.set_ylabel(uncertainty_column + '(mm2)')
    if selected_site_id != '':
        plt.suptitle(uncertainty_column + ' for Site ' + selected_site_id)
        plt.savefig('/Users/saraawad/Workspace/agriculture/Analysis&Interpretibility/Plotting/output/' + uncertainty_column + '_' + selected_site_id)
    else:
        plt.suptitle(uncertainty_column)
        plt.savefig('/Users/saraawad/Workspace/agriculture/Analysis&Interpretibility/Plotting/output/' + uncertainty_column)
    plt.show()

def plot_target_variance(path, model_type, isMDN=False):
    df = pd.read_csv(path + ".csv", delimiter=',')
    columns = ['Site Id', 'Date', 'Aleatoric Uncertainty',
        'Epistemic Uncertainty', 'Total Uncertainty', 'LE_bowen_corr(mm)',
            'LE_bowen_corr(mm)', 'LE_bowen_corr(mm)_predicted']
    df = df[columns]
    df = df[:100]
    actual_column = df['LE_bowen_corr(mm)']
    print("is MDN", isMDN)
    # if not isMDN:
    #     predicted_column = df['LE_bowen_corr(mm)_predicted']
    # else:
    predicted_column = df['LE_bowen_corr(mm)_predicted']
    uncertainty_column = df['Total Uncertainty']
    plt.plot(list(range(1, len(actual_column) + 1)), actual_column, color='b', label='actual')
    # plt.plot(list(range(1, len(actual_column) + 1)), predicted_column, color='r', label='predicted')
    plt.plot(list(range(1, len(actual_column) + 1)), uncertainty_column, color='g', label='uncertainty')
    plt.suptitle(model_type + " Target and Total Variance")
    # plt.savefig(self.output_path + file_name + '_actual_vs_predicted')
    plt.show()
    plt.close()

## Start plotting here
paths = ['ensemble_data_fs2', 'mcdropout_data_fs2']
types = ['Ensemble', 'MCDropout']
columns = ['Site Id', 'Date', 'Aleatoric Uncertainty',
       'Epistemic Uncertainty', 'Total Uncertainty']
num_rows = 100
uncertainty_columns = ['Aleatoric Uncertainty', 'Epistemic Uncertainty', 'Total Uncertainty']
# unique_sites = get_unique_sites(paths)
# print(unique_sites)

## Take all the testing rows
# df = pd.read_csv(paths[0] + ".csv", delimiter=',')
# num_rows = df.shape[0]

## Plot all data for each uncertainty type and each model
site_id = ''
for uncertainty_column in uncertainty_columns:
    df_list = get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows, site_id)
    plot_uncertainty(df_list, uncertainty_column, site_id)

# Plot for each site each uncertainty type per model for 100 rows
# for site_id in unique_sites:
#     for uncertainty_column in uncertainty_columns:
#         df_list = get_models_uncertainty(paths, types, columns, uncertainty_column, num_rows, site_id)
#         plot_uncertainty(df_list, uncertainty_column, site_id)

## Compare variance and mean 
# isMDN = False
# for i in range(len(paths)):
#     path = paths[i]
#     model_type = types[i]
#     if model_type == 'MDN':
#         isMDN = True
#     plot_target_variance(path, model_type, isMDN)


#### Testing
# df = pd.read_csv(paths[0] + ".csv", delimiter=',')
# columns = ['Site Id', 'Date', 'Aleatoric Uncertainty',
#         'Epistemic Uncertainty', 'Total Uncertainty', 'LE_bowen_corr(mm)',
#             'LE_bowen_corr(mm)', 'LE_bowen_corr(mm)_predicted']
# df = df[columns]
# df = df[:100]
# actual_column = df['LE_bowen_corr(mm)']
# predicted_column = df['LE_bowen_corr(mm)_predicted']
# uncertainty_column = df['Total Uncertainty']

# def listToStringWithoutBrackets(list1):
#     return (list1).replace('[','').replace(']','')

# predictedup_column = predicted_column
# # np.array(listToStringWithoutBrackets(predicted_column))
# actualup_column = actual_column
# np.array(actual_column)
# print(type(predictedup_column))
# print(type(actualup_column))
# print("-------------------------")
# # print(np.array(predictedup_column))
# # print(predicted_column)
# plt.plot(list(range(1, len(actualup_column) + 1)), actualup_column, color='b', label='actual')
# plt.plot(list(range(1, len(actualup_column) + 1)), predictedup_column[0], color='r', label='predicted')
# plt.plot(list(range(1, len(actual_column) + 1)), uncertainty_column, color='g', label='uncertainty')
# plt.show()
# plt.suptitle(model_type + " Target and Total Variance")




