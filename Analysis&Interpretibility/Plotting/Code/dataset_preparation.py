
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
import datetime
import json
import time
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
import category_encoders as ce

from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.metrics.cluster import normalized_mutual_info_score
from scipy.stats.stats import pearsonr, spearmanr
from scipy.spatial import distance
from collections import OrderedDict

import warnings
warnings.filterwarnings("ignore")

#rpy
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
import rpy2
import rpy2.robjects.packages as rpackages
import rpy2.robjects as ro
pandas2ri.activate()
import rpy2.robjects.numpy2ri
rpy2.robjects.numpy2ri.activate()
from rpy2.robjects.conversion import localconverter
import warnings
from rpy2.rinterface import RRuntimeWarning
warnings.filterwarnings("ignore", category=RRuntimeWarning)
pandas2ri.activate()

rpy2.robjects.numpy2ri.activate()
runit = robjects.r
print(runit)
runit['source']('smogn.R')

scores_dict = OrderedDict()


class DataPreparation():
#     input_path = '/Users/saraawad/Desktop/Modules Code/'
#     output_path = '/Users/saraawad/Desktop/Modules Code/Uncertainty/Output/'
#     output_column = "LE_bowen_corr(mm)"

    # input_path = '/Users/saraawad/Desktop/Modules Code/'
    # output_path = '/Users/saraawad/Desktop/Modules Code/Uncertainty/evaluating_bdl-master/ensemble/'
    experiment_name = "Ensemble-MAP-Adam_1"

    training_time = 0
    input_path = '/apps/data/'
    output_path = '/apps/output/Ensemble/'
    output_column = "LE_bowen_corr(mm)"
    climate = 'Csa'

#     def __init__(self):
#          self.read_data()

    def read_data(self):
        file_path = self.input_path + "All_Manual_Daily_Albedo_NDVI_LST_Cleaned.csv"
        # file_path = self.input_path + "Kmeans_WS_Cluster_1.csv"
        df = pd.read_csv(file_path, delimiter=',')

        columnsToDrop = ['Year', 'Month', 'Day', 'Site Id_1', 'Site Id_2', 'Site Id_3',
                        'Site Id_4', 'Site Id_5', 'Site Id_6','Vegetation', 'Latitude', 'Longitude',
                        'G','G-1','G-2','G-3','G-4','G-5','Climate',
                        'Climate_1', 'Climate_2', 'Climate_3',
                        'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
                        'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
                        'Longitude_5', 'Longitude_6',
                        'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
                        'H_bowen_corr-5', 'C_BOWENS',
                        'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
                        'LE', 'LE_bowen_corr',
                        'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
                        'Elevation(m)_5', 'Elevation(m)_6',
                        'ETo', 'EToF', 'ETr', 'ETrF', 'ETo', 'SW_IN']

        df = df.drop(columnsToDrop, axis = 1)
        df.dropna(inplace=True)

        df = df[df[self.output_column].between(1, 15)]
#         df = df[df['Climate'] == climate]

        #Generate 5 lags
        lagsForColumns = ["SW_IN", "WS", "RH", "TA", "EEflux LST", "EEflux Albedo", "EEflux NDVI"]
        df = self.generate_lags(df, lagsForColumns)
        df.dropna(inplace=True)

        #Binary Encode Site Id
        df = self.binary_encode_column(df, "Site Id")

        #Sort data by Site Id and date
#         df['Date'] = pd.to_datetime(df.Date)
        # df.sort_values(by=['Date'], inplace=True, ascending=True)
        
        #Drop columns
        df.drop(columns=['Site Id_0'], inplace=True)
        # df.sort_values(by=['Date', 'Site Id'], inplace=True, ascending=True)
        return df

    def binary_encode_column(self, df, columnToEncode):
        encoder = ce.BinaryEncoder(cols=[columnToEncode])
        df_encoder = encoder.fit_transform(df[columnToEncode])
        df = pd.concat([df, df_encoder], axis=1)
        return df

    def generate_lags_for(self, df, column, lags_count): 
            for i in range(lags_count):
                lag_name = column + "-" + str(i + 1)
                df[lag_name] = df[column].shift(i + 1)
            return df

    def generate_lags(self, df, lagsForColumns):
        for k in range(len(lagsForColumns)):
            col = lagsForColumns[k]
            if col in df.columns:
                df = self.generate_lags_for(df, col, 5)
        return df

    # Calculate error metrics
    def error_metrics(self, y_test, y_pred, average_output, data_size, nb_columns=48, nb_params=1):
        n = len(y_test)
        #convert to float for it to work for AIC and BIC data should all be of the same format i.e float not mix of types
        y_test_f =  [float(item) for item in y_test.values]
        y_predict_f =  [float(item) for item in y_pred]
        test_scores = {}
        r2_Score = r2_score(y_test, y_pred)  # r-squared
        adjusted_r2 = 1 - ((1 - r2_Score) * (n - 1)) / (n - nb_columns - 1)  # adjusted r-squared
        rmse_score = np.sqrt(mean_squared_error(y_test, y_pred))  # RMSE
        mse_score = mean_squared_error(y_test_f, y_predict_f)  # MSE
        mae_score = mean_absolute_error(y_test, y_pred)  # MAE
        mape_score = np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64).mean() * 100  # MAPE
        re = (mse_score / np.mean(y_pred)) * 100
        accuracy = 100 - np.mean(mape_score)
        spearman_corr, _ = spearmanr(y_test, y_pred)
        r2 = str(round(r2_Score, 2))
        adjusted_r2 = str(round(adjusted_r2, 2))
        mae = str(round(mae_score, 2))
        mse = str(round(mse_score, 2))
        rmse = str(round(rmse_score, 2))
        re = str(round(re, 2))
        spearman = str(round(spearman_corr, 2))
        mape = str(round(mape_score, 2))
        accuracy = str(round(accuracy, 2))
        nmi = normalized_mutual_info_score(np.array(y_test_f), np.array(y_predict_f))

        def calculate_aic(n, mse, num_params):
            aic = n * np.log(mse) + 2 * num_params
            return aic

        def calculate_bic(n, mse, num_params):
            bic = n * np.log(mse) + num_params * np.log(n)
            return bic

        aic = str(round(calculate_aic(len(y_test), mse_score, nb_params), 2))
        bic = str(round(calculate_bic(len(y_test), mse_score, nb_params), 2))

        pearson_corr, _ = pearsonr(np.array(y_test_f), np.array(y_predict_f))
        distance_corr = distance.correlation(np.array(y_test_f), np.array(y_predict_f))
        
        test_scores["Average"] = str(average_output)
        test_scores["Average Testing"] = str(np.mean(y_test))
        test_scores["R2"] = r2
        test_scores["Adjusted R2"] = adjusted_r2
        test_scores["RMSE"] = rmse
        test_scores["MSE"] = mse
        test_scores["MAE"] = mae
        test_scores["MAPE"] = mape
        test_scores["Accuracy"] = accuracy
        test_scores["Pearson C.C."] = pearson_corr
        test_scores["Spearman C.C."] = spearman
        test_scores["Spatial Distance"] = distance_corr
        test_scores["NMI"] = nmi
        test_scores["Data Size"] = str(data_size)
        test_scores["AIC"] = aic
        test_scores["BIC"] = bic
        test_scores["Relative Error"] = re
        return test_scores

    def write_dict_to_json(self, path, content):
        with open(path + '.json', 'w') as file:
            file.write(json.dumps(content, ensure_ascii=False))
    
    def write_str_to_txt(self, path, content):
        text_file = open(path + ".txt", "w")
        text_file.write(content) 
        text_file.close()

    def plot_actual_vs_predicted(self, actual_column, predicted_column, file_name=""):
        plt.plot(list(range(1, len(actual_column) + 1)), actual_column, color='b', label='actual')
        plt.plot(list(range(1, len(actual_column) + 1)), predicted_column, color='r', label='predicted')
        plt.legend(loc='best')
        plt.suptitle('Actual vs. Predicted')
        plt.savefig(self.output_path + file_name + '_actual_vs_predicted')
        plt.close()
        
    def plot_actual_vs_predicted_scatter_bisector(self, actual_column, predicted_column, file_name=""):
        fig, ax = plt.subplots()
        ax.scatter(actual_column, predicted_column)
        ax.plot([actual_column.min(), actual_column.max()], [predicted_column.min(), predicted_column.max()], 'k--', lw=4)
        ax.set_xlabel('Actual')
        ax.set_ylabel('Predicted')
        plt.suptitle('Actual vs. Predicted Scatter Plot')
        plt.savefig(self.output_path + file_name + '_actual_vs_predicted_scatter_plot')
        plt.close()

    def export_scores(self, file_name, scores, columnName):
        if not os.path.exists(file_name):
            df = pd.DataFrame(list())
            df.to_csv(file_name, index=False)
        else:
            df = pd.read_csv(file_name, delimiter=',')
        df["Error Metrics"] = scores.keys()
        df[columnName] = scores.values()
        df.to_csv(file_name, index=False)
        return df

    def split_train_test_valid(self, df, TRAIN_RATIO, TEST_RATIO):
        X_train = pd.DataFrame()
        X_test = pd.DataFrame()
        X_valid = pd.DataFrame()
        Y_train = pd.DataFrame()
        Y_test = pd.DataFrame()
        Y_valid = pd.DataFrame()
        unique_sites = df["Site Id"].unique()
        print("Number of sites:", len(unique_sites))
        for site in unique_sites:
            df_site = df[df["Site Id"] == site]
            X = df_site
            train_index = int(X.shape[0] * TRAIN_RATIO)
            test_index = int(X.shape[0] * (TRAIN_RATIO + TEST_RATIO))
            X_train = X_train.append(X[:train_index], ignore_index = True)
            X_test = X_test.append(X[train_index:test_index], ignore_index = True)
            X_valid = X_valid.append(X[test_index:], ignore_index = True)
            
            Y_train = Y_train.append(X[:train_index], ignore_index = False)
            Y_test = Y_test.append(X[train_index:test_index], ignore_index = False)
            Y_valid = Y_valid.append(X[test_index:], ignore_index = False)
        
        Y_train = Y_train[[self.output_column]]
        Y_test = Y_test[[self.output_column]]
        Y_valid = Y_valid[[self.output_column]]
        
        X_train = X_train.drop([self.output_column], axis = 1) 
        X_test = X_test.drop([self.output_column], axis = 1) 
        X_valid = X_valid.drop([self.output_column], axis = 1) 
        
        return (X_train, Y_train, X_test, Y_test, X_valid, Y_valid)

    def split_train_test(self, df, TRAIN_RATIO):
        X_train = pd.DataFrame()
        X_test = pd.DataFrame()
        Y_train = pd.DataFrame()
        Y_test = pd.DataFrame()
        
        unique_sites = df["Site Id"].unique()
        print("Number of sites:", len(unique_sites))

        for site in unique_sites:
            df_site = df[df["Site Id"] == site]
            X = df_site
            train_index = int(X.shape[0] * TRAIN_RATIO)

            X_train = X_train.append(X[:train_index], ignore_index = True)
            X_test = X_test.append(X[train_index:], ignore_index = True)
            Y_train = Y_train.append(X[:train_index], ignore_index = True)
            Y_test = Y_test.append(X[train_index:], ignore_index = True)
       
        Y_train = Y_train[[self.output_column]]
        Y_test = Y_test[[self.output_column]]
       
        X_train = X_train.drop([self.output_column], axis = 1)
        X_test = X_test.drop([self.output_column], axis = 1)
       
        return X_train, Y_train, X_test, Y_test


    def drop_columns(self, X, columnsToDrop):
        for columnToDrop in columnsToDrop:
            X.drop([columnToDrop], axis = 1, inplace=True)
        return X

    def scale_min_max(self, X_train, X_test, X_valid=None):
        print("scale using min max")
        scaler = MinMaxScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)
        if X_valid is not None:
            X_valid_scaled = scaler.transform(X_valid)
            return X_train_scaled, X_test_scaled, X_valid_scaled
        else:
            return X_train_scaled, X_test_scaled
    
    def scale_zscore(self, X_train, X_test, X_valid=None):
        print("scale using zscore")
        scaler = StandardScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)
        if X_valid is not None:
            X_valid_scaled = scaler.transform(X_valid)
            return X_train_scaled, X_test_scaled, X_valid_scaled
        else:
            return X_train_scaled, X_test_scaled
    def process_data(self, df, scaleMinMax = True):
        X_train, Y_train, X_test, Y_test, X_valid, Y_valid = self.split_train_test_valid(df, 0.6, 0.2)
        # Export data sets for training and validation only, testing is further exported with prediction
        columns = df.columns
        _ = self.prepare_dataset_export(X_train, Y_train, columns)
        _ = self.prepare_dataset_export(X_valid, Y_valid, columns, name="validation_dataset.csv")
        X_test_exp = self.prepare_dataset_export(X_test, Y_test, columns, export=False)

        # Drop columns and scale data
        columnsToDrop = ["Site Id", "Date"]
        X_train = self.drop_columns(X_train, columnsToDrop)
        X_test = self.drop_columns(X_test, columnsToDrop)
        X_valid = self.drop_columns(X_valid, columnsToDrop)
        # We will scale min max or using standardization z-score
        if scaleMinMax:
            X_train_scaled, X_test_scaled, X_valid_scaled = self.scale_min_max(X_train, X_test, X_valid)
        else:
            X_train_scaled, X_test_scaled, X_valid_scaled = self.scale_zscore(X_train, X_test, X_valid)
        return  X_train_scaled, Y_train, X_test_scaled, Y_test, X_valid_scaled, Y_valid, X_test_exp 

    def prepare_dataset_export(self, X, Y, column_names, export=True, name="train_dataset.csv"):
        X_df = pd.DataFrame(X, columns=column_names)
        X_df[self.output_column] = np.array(Y)
        if export:
             X_df.to_csv(self.output_path + name)

        return X_df

    def export_test_dataset(self, X, predicted_output, new_columns = [], new_columns_names=[], extra_name = ''):
        X[self.output_column + "_predicted"] = predicted_output
        if len(new_columns) > 0 and (len(new_columns) == len(new_columns_names)):
            for i in range(len(new_columns)):
                new_column_name = new_columns_names[i]
                new_column = new_columns[i]
                X[new_column_name] = np.array(new_column)
        X.to_csv(self.output_path + 'test_dataset_' + str(extra_name) + '.csv')
        return X

    def process_unscaled_data(self, df):
        X_train, Y_train, X_test, Y_test, X_valid, Y_valid = self.split_train_test_valid(df, 0.6, 0.2)
        
        #Export data sets for training and validation only, testing is further exported with prediction
        columns = df.columns

        _ = self.prepare_dataset_export(X_train, Y_train, columns)
        _ = self.prepare_dataset_export(X_valid, Y_valid, columns, name="validation_dataset.csv")
        X_test_exp = self.prepare_dataset_export(X_test, Y_test, columns, export=False)

        columnsToDrop = ["Site Id", "Date"]
        X_train = self.drop_columns(X_train, columnsToDrop)
        X_test = self.drop_columns(X_test, columnsToDrop)
        X_valid = self.drop_columns(X_valid, columnsToDrop)
        return  X_train, Y_train, X_test, Y_test, X_valid, Y_valid, X_test_exp
    
    def process_train_test_data(self, df, scaleMinMax = True):
        X_train, Y_train, X_test, Y_test = self.split_train_test(df, 0.8)
        print("X train", X_train.shape, "X test", X_test.shape)
        print("Y train", Y_train.shape, "Y test", Y_test.shape)
        # Export data sets for training, testing is further exported with prediction
        columns = df.columns
        print("columns:", columns, len(columns))

        _ = self.prepare_dataset_export(X_train, Y_train, columns)
        X_test_exp = self.prepare_dataset_export(X_test, Y_test, columns, export=False)

        # Drop columns and scale data
        columnsToDrop = ["Site Id", "Date"]
        X_train = self.drop_columns(X_train, columnsToDrop)
        X_test = self.drop_columns(X_test, columnsToDrop)
        # We will scale min max or using standardization z-score
        if scaleMinMax:
            X_train_scaled, X_test_scaled = self.scale_min_max(X_train, X_test, None)
        else:
            X_train_scaled, X_test_scaled = self.scale_zscore(X_train, X_test, None)
        return  X_train_scaled, Y_train, X_test_scaled, Y_test, X_test_exp 


    def plot_learning_curve(self, history, index):
        plt.plot(history.history['mse'])
        plt.plot(history.history['val_mse'])
        plt.title('MSE Loss')
        plt.ylabel('mse')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        plt.savefig(self.output_path + 'learning_curve_train_val_mse_' + str(index))
        plt.close()

        plt.plot(history.history['mae'])
        plt.plot(history.history['val_mae'])
        plt.title('MAE Loss')
        plt.ylabel('mae')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        plt.savefig(self.output_path +  'learning_curve_train_val_mae_' + str(index))
        plt.close()

        plt.plot(history.history['mape'])
        plt.plot(history.history['val_mape'])
        plt.title('MAPE Loss')
        plt.ylabel('mape')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        plt.savefig(self.output_path +  'learning_curve_train_val_mape_' + str(index))
        plt.close()

        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Loss')
        plt.ylabel('mape')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        plt.savefig(self.output_path +  'learning_curve_train_val_loss_' + str(index))
        plt.close()
        
    def plot_variance(self, Y_true, Y_predict, mean, sigma, sample):
        Y_true = np.array(Y_true)
        Y_predict = Y_predict.reshape(-1, 1)
        plt.figure(1)
        plt.plot(Y_true[:sample], 'bo')
        plt.plot(Y_predict[:sample], 'ro')
        plt.vlines(x = np.arange(0, sample), ymin = mean[:sample] - sigma[:sample], ymax = mean[:sample] + sigma[:sample], color = 'g', linewidth=2.0)
        plt.legend(['Real data', 'Mu', 'mu+-sig'], loc = 'best')
        plt.title('Ground Truth vs Prediction')
        plt.xlim([-1, sample])
        plt.savefig(self.output_path +  'mcdropout_variance')
        plt.show()
        plt.close()

    def plot_regression_model_analysis(self, gt=None,  trn=None, tst=None, pred=None, xlim=None, ylim=None, title=None):
        if gt:
            x_gt, y_gt = gt
            plt.plot(x_gt, y_gt, c='#F0AA00', 
                    alpha=.8, lw=2, label="ground truth")
        if trn:
            x_trn, y_trn = trn
            plt.scatter(x_trn,  y_trn,
                        s=8, ec='black', lw=1, fc=None, alpha=1,
                        label='train samples')
        
        if tst:
            x_tst, y_tst = tst
            plt.scatter(x_tst, y_tst, s=5, c='blue', alpha=.1, label='test samples')

        if pred:
            x_tst, yhat_mean, yhat_std = pred
            plt.scatter(x_tst, yhat_mean, s=5, c='magenta', alpha=1, label='preds')
            if yhat_std is not None:
                plt.fill_between(x_tst, (yhat_mean - 1.*yhat_std), (yhat_mean + 1.*yhat_std), lw=1,
                                ec='blue', fc='blue', alpha=.3, label='preds 1*std')
                plt.fill_between(x_tst, (yhat_mean - 2.*yhat_std), (yhat_mean + 2.*yhat_std), lw=1,
                                ec='blue', fc='blue', alpha=.2, label='preds 2*std')

        if xlim:
            plt.xlim(*xlim)
            
        if ylim:
            plt.ylim(*ylim)
        
        if title:
            plt.title(title)
            
        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend(bbox_to_anchor=(1.35, 1.03), loc='upper right', fancybox=False, framealpha=1.0)
        plt.savefig(self.output_path +  'mcdropout')
        plt.close()
        
       #evaluate ub error metrics
    def evaluate(self, average_target, data_size, nb_columns, actual, predicted, num_params, thresh, rel_method='extremes', extr_type='high', coef=1.5, relevance_pts=None):
        y = np.array(actual)
        phi_params, loss_params, _ = self.get_phi_loss_params(y, rel_method, extr_type, coef, relevance_pts)

        return self.get_stats(average_target, actual, predicted, nb_columns, thresh, phi_params, loss_params, data_size, num_params)

    def get_phi_loss_params(self, y, rel_method, extr_type='high', coef=1.5, relevance_pts=None):

        # get the parameters of the relevance function
        # param df: dataframe being used
        # param target_variable: name of the target variable
        # param rel_method: either 'extremes' or 'range'
        # param extr_type: either 'high', 'low', or 'both' (defualt)
        # param coef: default: 1.5
        # param relevance_pts: the relevance matrix in case rel_method = 'range'
        # return: phi parameters and loss parameters


        if relevance_pts is None:
            print('Will not use relevance matrix')
            params = runit.get_relevance_params_extremes(y, rel_method=rel_method, extr_type=extr_type, coef=coef)
        else:
            print('Using supplied relevance matrix')
            params = runit.get_relevance_params_range(y, rel_method=rel_method, extr_type=extr_type, coef=coef,
                                                    relevance_pts=relevance_pts)

        # phi params and loss params
        phi_params = params[0]
        loss_params = params[1]
        relevance_values = params[2]

        phi_params = dict(zip(phi_params.names, list(phi_params)))
        loss_params = dict(zip(loss_params.names, list(loss_params)))

        return phi_params, loss_params, relevance_values


    def get_stats(self, average_target, y_test, y_pred, nb_columns, thr_rel, phi_params, loss_params, data_size, num_params=1):

        # Function to compute regression error metrics between actual and predicted values +
        # correlation between both using different methods: Pearson, Spearman, and Distance
        # param y_test: the actual values. Example df['actual'] (the string inside is the name
        # of the actual column. Example: df['LE (mm)'], df['demand'], etc.)
        # param y_pred: the predicted vlaues. Example df['predicted']
        # param nb_columns: number of columns <<discarding the target variable column>>
        # return: R2, Adj-R2, RMSE, MSE, MAE, MAPE
        
        #convert to float for it to work for AIC and BIC data should all be of the same format i.e float not mix of types
        y_test_f =  [float(item) for item in y_test]
        y_predict_f =  [float(item) for item in y_pred]

        def mean_absolute_percentage_error(y_true, y_pred):
            y_true, y_pred = np.array(y_true), np.array(y_pred)
            return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

        if not isinstance(y_test, list):
            y_test = list(y_test)
        if not isinstance(y_pred, list):
            y_pred = list(y_pred)

        n = len(y_test)

        r2_Score = r2_score(y_test, y_pred)  # r-squared
        adjusted_r2 = 1 - ((1 - r2_Score) * (n - 1)) / (n - nb_columns - 1)  # adjusted r-squared
        rmse_score = np.sqrt(mean_squared_error(y_test, y_pred))  # RMSE
        mse_score = mean_squared_error(y_test, y_pred)  # MSE
        mae_score = mean_absolute_error(y_test, y_pred)  # MAE
        mape_score = np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64).mean() * 100  # MAPE
        accuracy_score = 100 - np.mean(mape_score) # Accuracy
        
        def calculate_aic(n, mse, num_params):
            aic = n * np.log(mse) + 2 * num_params
            return aic

        def calculate_bic(n, mse, num_params):
            bic = n * np.log(mse) + num_params * np.log(n)
            return bic

        aic = str(round(calculate_aic(len(y_test), mse_score, num_params), 2))
        bic = str(round(calculate_bic(len(y_test), mse_score, num_params), 2))

        nmi = normalized_mutual_info_score(np.array(y_test_f), np.array(y_predict_f))

        trues = np.array(y_test)
        preds = np.array(y_pred)

        method = phi_params['method']
        npts = phi_params['npts']
        controlpts = phi_params['control.pts']
        ymin = loss_params['ymin']
        ymax = loss_params['ymax']
        tloss = loss_params['tloss']
        epsilon = loss_params['epsilon']

        rmetrics = runit.eval_stats(trues, preds, thr_rel, method, npts, controlpts, ymin, ymax, tloss, epsilon)

        # create a dictionary of the r metrics extracted above
        rmetrics_dict = dict(zip(rmetrics.names, list(rmetrics)))

        if isinstance(y_pred[0], np.ndarray):
            y_pred_new = [x[0] for x in y_pred]
            y_pred = y_pred_new

        pearson_corr, _ = pearsonr(np.array(y_test_f), np.array(y_predict_f))
        distance_corr = distance.correlation(np.array(y_test_f), np.array(y_predict_f))
        spearman_corr, _ = spearmanr(y_test, y_pred)

        print('\nUtility Based Metrics')
        print('F1: %.5f' % rmetrics_dict['ubaF1'][0])
        print('F2: %.5f' % rmetrics_dict['ubaF2'][0])
        print('F05: %.5f' % rmetrics_dict['ubaF05'][0])
        print('precision: %.5f' % rmetrics_dict['ubaprec'][0])
        print('recall: %.5f' % rmetrics_dict['ubarec'][0])

        print('\nRegression Error Metrics')
        print('Average of test output:',  str(np.mean(y_test)))
        print('Average of output:', str(average_target))
        print('R2: %.2f' % r2_Score)
        print('Adj-R2: %.2f' % adjusted_r2)
        print('RMSE: %.2f' % rmse_score)
        print('MSE: %.2f' % mse_score)
        print('MAE: %.2f' % mae_score)
        print('MAPE: %.2f' % mape_score)
        print('Accuracy: %.2f' % accuracy_score)
        print('AIC:', aic)
        print('BIC:',  bic)
        print('Normalized Mutal Information:', nmi)

        print('\nCorrelations')
        print('Pearson: %.2f' % pearson_corr)
        print('Spearman: %.2f' % spearman_corr)
        print('Distance: %.2f' % distance_corr)
        print('Data Size:', str(data_size))

        scores_dict["F1"] = rmetrics_dict['ubaF1'][0]
        scores_dict["F2"] = rmetrics_dict['ubaF2'][0]
        scores_dict["F05"] = rmetrics_dict['ubaF05'][0]
        scores_dict["Precision"] = rmetrics_dict['ubaprec'][0]
        scores_dict["Recall"] = rmetrics_dict['ubarec'][0]
        scores_dict["Average"] = str(average_target)
        scores_dict["Average Testing"] = str(np.mean(y_test))
        scores_dict["R2"] = r2_Score
        scores_dict["Adjusted R2"] = adjusted_r2
        scores_dict["RMSE"] = rmse_score
        scores_dict["MSE"] = mse_score
        scores_dict["MAE"] = mae_score
        scores_dict["MAPE"] = mape_score
        scores_dict["Accuracy"] = accuracy_score
        scores_dict["Pearson C.C."] = pearson_corr
        scores_dict["Spearman C.C."] = spearman_corr
        scores_dict["Spatial Distance"] = distance_corr
        scores_dict["NMI"] = nmi
        scores_dict["AIC"] = aic
        scores_dict["BIC"] = bic
        scores_dict["Data Size"] = data_size

        return scores_dict
