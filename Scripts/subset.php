<?php

$withColumn = array_map('str_getcsv', file('/Users/saraawad/Desktop/joint_manual_cleaned.csv'));
$withoutColumn = array_map('str_getcsv', file('/Users/saraawad/Desktop/test_dataset.csv'));

$resultSet = common_set($withoutColumn, $withColumn);

// Write to CSV File: 
// $csv = array();
// $csv[] = implode(',', $withoutColumn[0]);
// for ($i = 0; $i < count($resultSet); $i++) 
//     $csv[] = implode(',', $resultSet[$i]);

// file_put_contents('output.csv', implode("\n", $csv));


function common_set($set, $subSet) {

    var_dump("Objects in big set: " . count($set));
    var_dump("Objects in small set: " . count($subSet));
    var_dump("Max Count in result set: " . (count($set) - count($subSet)));

    $resultSet = array();
    $diffMap = array();
    
    //joint
    $objectSetSmall = array();
    for ($i = 0; $i < count($subSet); $i++) {
        $row = array(
            //Date
            $subSet[$i][0],
            //Site ID
            $subSet[$i][1],
            //LE bowen
            // $subSet[$i][2]
        );


        $key = md5(implode('-',$row));
        
        //indicate that key exist in joint
        $diffMap[$key] = true;
        $objectSetSmall[$key] = $subSet[$i];

    }


    $objectSetBig = array();
    for ($i = 0; $i < count($set); $i++) {

        $row = array(
            $set[$i][0],
            $set[$i][1],
            // $set[$i][2]
        );

        // var_dump($set[$i]);
        // exit;

        $key = md5(implode('-',$row));
        $objectSetBig[$key] = $set[$i];
    }

    $diffedObjects = array();
    foreach($objectSetBig as $key => $object) {
        if (isset($objectSetSmall[$key])) {
            $smallSetObj = $objectSetSmall[$key];
            $bigSetObj = $objectSetBig[$key];
            //13 is indx for eefluxET
            $bigSetObj[] = $smallSetObj[13];
            $diffedObjects[] = $bigSetObj;
        }
    }

    // Write to CSV File: 
    $csv = array();
    $cols = $set[0];
    $cols[] = $subSet[0][13];
    $csv[] = implode(',', $cols);

    for ($i = 0; $i < count($diffedObjects); $i++) {
        $row = implode(',', $diffedObjects[$i]);
        $csv[] = $row;
    }

    var_dump('outputed ' . count($diffedObjects) . ' csv records');
    file_put_contents('output_residual.csv', implode("\n", $csv));

    return $resultSet;
}

