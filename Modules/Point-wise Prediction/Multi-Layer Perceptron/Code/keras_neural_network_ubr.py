##Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
import json
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
import category_encoders as ce
import math
from math import *

import tensorflow
print(tensorflow.__version__)
# from tensorflow import keras
from tensorflow.keras.layers import Dense, Input, Dropout
from tensorflow.keras.models import Sequential
from tensorflow import feature_column
from keras.wrappers.scikit_learn import KerasRegressor
from tensorflow.keras.layers import BatchNormalization
from sklearn.model_selection import GridSearchCV
from tensorflow.keras.optimizers import SGD
from tensorflow.keras import layers
from keras.regularizers import l2
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from tensorflow.keras.models import load_model
from keras.models import model_from_json
from collections import OrderedDict
from tensorboard.plugins.hparams import api as hp
from math import log
import tensorflow.keras.backend as K
print("GPU Available: ", tensorflow.test.is_gpu_available())


from scipy.spatial import distance
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
import rpy2
import rpy2.robjects.packages as rpackages
import rpy2.robjects as ro
pandas2ri.activate()
import rpy2.robjects.numpy2ri
rpy2.robjects.numpy2ri.activate()
from rpy2.robjects.conversion import localconverter
import warnings
from rpy2.rinterface import RRuntimeWarning
warnings.filterwarnings("ignore", category=RRuntimeWarning)
pandas2ri.activate()
from matplotlib import pyplot
from collections import Counter
from sklearn.model_selection import StratifiedKFold, KFold
from numpy import mean
from numpy import std
from pandas import read_csv
from sklearn.preprocessing import RobustScaler, OneHotEncoder, MinMaxScaler, PowerTransformer, StandardScaler
from scipy.stats import normaltest
from sklearn.model_selection import ParameterSampler
from numpy.random import randn
from scipy.stats import shapiro
from sklearn.model_selection import train_test_split
from numpy import *
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from scipy.stats.stats import pearsonr, spearmanr
from sklearn.metrics.cluster import normalized_mutual_info_score
from scipy import stats
import tensorflow as tf
import multiprocessing as mp
import time
import os
import collections
import matplotlib.pyplot as plt
import itertools as it
from math import log
import tensorflow.keras.backend as K
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import re
import warnings
warnings.filterwarnings("ignore")
from sklearn.utils import shuffle


########################################################################################################################
                                            #Setting Global Variables
########################################################################################################################

#specify path of dataset
user_path = "/home/sba19/scratch/smogn/"
user_path = '/apps/data/'
output_path = '/apps/output/BaseMLP/'
input_path = user_path + "All_Manual_Daily_Albedo_NDVI_LST_Cleaned.csv"

scores_dict = OrderedDict()

# experiment_path = user_path + "Experiment Details.csv"


#specify saved file directory 
# output_path = user_path + "output/daily/"
# output_result_path = output_path + "Daily/Experiment1/"


#specify one-hot encoded vector names
#For now we use month, vegetation and site id
one_hot_encoded = ['Site Id_1', 'Site Id_2', 'Site Id_3', 'Site Id_4', 'Site Id_5',
'Site Id_6', 'Month_1', 'Month_2', 'Month_3', 'Month_4','Vegetation_1', 'Vegetation_2','Vegetation_3']
                  
#specify desired split size
test_size = 0.2
  
#specify the option of utility based
utility_based = True

#specify if random search
random_search = False
#specify if you wish to apply over sampling by smogn
smogn = False

target_variable = "LE_bowen_corr(mm)"

#For daily
rel_method='range'
extr_type='high'
coef=1.5

relevance_pts=np.array([
    [1, 0 , 0],
    [4, 0 , 0],
    [15, 1 , 0]
])
rel="auto"
thr_rel=0.1
Cperc=np.array([1,1.2])
k=5
repl=False
dist="Euclidean"
p=2
pert=0.1

#For joint
# rel_method='extremes'
# extr_type='high'
# coef=1.5
# rell = None
# relevance_pts=rell
# rel="auto"
# thr_rel=0.5
# Cperc=np.array([1,1.2])
# k=5
# repl=False
# dist="Euclidean"
# p=2
# pert=0.1


########################################################################################################################
                                            #Helper Methods
########################################################################################################################

#evaluate ub error metrics
def evaluate(df, actual, predicted, num_params, thresh=thr_rel, rel_method=rel_method, extr_type=extr_type, coef=coef, relevance_pts=relevance_pts):
    y = np.array(df[actual])
    phi_params, loss_params, _ = get_phi_loss_params(y, rel_method, extr_type, coef, relevance_pts)

    nb_columns = len(list(df.columns.values)) - 1

    mape_score,distance_corr,spearman_corr,pearson_corr,mae_score,mse_score,rmse_score,adjusted_r2,r2_Score,f1,f2,f5,prec,recall, accuracy, aic, bic, nmi = get_stats(df[actual], df[predicted], nb_columns, thresh, phi_params, loss_params, num_params)
    return mape_score,distance_corr,spearman_corr,pearson_corr,mae_score,mse_score,rmse_score,adjusted_r2,r2_Score,f1,f2,f5,prec,recall, accuracy, aic, bic, nmi

def get_phi_loss_params(y, rel_method, extr_type='high', coef=1.5, relevance_pts=None):

    # get the parameters of the relevance function
    # param df: dataframe being used
    # param target_variable: name of the target variable
    # param rel_method: either 'extremes' or 'range'
    # param extr_type: either 'high', 'low', or 'both' (defualt)
    # param coef: default: 1.5
    # param relevance_pts: the relevance matrix in case rel_method = 'range'
    # return: phi parameters and loss parameters


    if relevance_pts is None:
        print('Will not use relevance matrix')
        params = runit.get_relevance_params_extremes(y, rel_method=rel_method, extr_type=extr_type, coef=coef)
    else:
        print('Using supplied relevance matrix')
        params = runit.get_relevance_params_range(y, rel_method=rel_method, extr_type=extr_type, coef=coef,
                                                  relevance_pts=relevance_pts)

    # phi params and loss params
    phi_params = params[0]
    loss_params = params[1]
    relevance_values = params[2]

    phi_params = dict(zip(phi_params.names, list(phi_params)))
    loss_params = dict(zip(loss_params.names, list(loss_params)))

    return phi_params, loss_params, relevance_values


def get_stats(y_test, y_pred, nb_columns, thr_rel, phi_params, loss_params, num_params):

    # Function to compute regression error metrics between actual and predicted values +
    # correlation between both using different methods: Pearson, Spearman, and Distance
    # param y_test: the actual values. Example df['actual'] (the string inside is the name
    # of the actual column. Example: df['LE (mm)'], df['demand'], etc.)
    # param y_pred: the predicted vlaues. Example df['predicted']
    # param nb_columns: number of columns <<discarding the target variable column>>
    # return: R2, Adj-R2, RMSE, MSE, MAE, MAPE
    
    #convert to float for it to work for AIC and BIC data should all be of the same format i.e float not mix of types
    y_test_f =  [float(item) for item in y_test.values]
    y_predict_f =  [float(item) for item in y_pred]

    def mean_absolute_percentage_error(y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    if not isinstance(y_test, list):
        y_test = list(y_test)
    if not isinstance(y_pred, list):
        y_pred = list(y_pred)

    n = len(y_test)

    r2_Score = r2_score(y_test, y_pred)  # r-squared
    adjusted_r2 = 1 - ((1 - r2_Score) * (n - 1)) / (n - nb_columns - 1)  # adjusted r-squared
    rmse_score = np.sqrt(mean_squared_error(y_test, y_pred))  # RMSE
    mse_score = mean_squared_error(y_test, y_pred)  # MSE
    mae_score = mean_absolute_error(y_test, y_pred)  # MAE
    #print(np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64))
    mape_score = np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64).mean() * 100  # MAPE
    accuracy_score = 100 - np.mean(mape_score) # Accuracy
    
    def calculate_aic(n, mse, num_params):
        aic = n * log(mse) + 2 * num_params
        return aic

    def calculate_bic(n, mse, num_params):
        bic = n * log(mse) + num_params * log(n)
        return bic

    aic = str(round(calculate_aic(len(y_test), mse_score, num_params), 2))
    bic = str(round(calculate_bic(len(y_test), mse_score, num_params), 2))

    # y_test_arr = np.array(y_test)
    nmi = normalized_mutual_info_score(np.array(y_test_f), np.array(y_predict_f))

    trues = np.array(y_test)
    preds = np.array(y_pred)

    method = phi_params['method']
    npts = phi_params['npts']
    controlpts = phi_params['control.pts']
    ymin = loss_params['ymin']
    ymax = loss_params['ymax']
    tloss = loss_params['tloss']
    epsilon = loss_params['epsilon']

    rmetrics = runit.eval_stats(trues, preds, thr_rel, method, npts, controlpts, ymin, ymax, tloss, epsilon)

    # create a dictionary of the r metrics extracted above
    rmetrics_dict = dict(zip(rmetrics.names, list(rmetrics)))

    if isinstance(y_pred[0], np.ndarray):
        y_pred_new = [x[0] for x in y_pred]
        y_pred = y_pred_new
    pearson_corr, _ = pearsonr(y_test, y_pred)
    spearman_corr, _ = spearmanr(y_test, y_pred)
    distance_corr = distance.correlation(y_test, y_pred)

    print('\nUtility Based Metrics')
    print('F1: %.5f' % rmetrics_dict['ubaF1'][0])
    print('F2: %.5f' % rmetrics_dict['ubaF2'][0])
    print('F05: %.5f' % rmetrics_dict['ubaF05'][0])
    print('precision: %.5f' % rmetrics_dict['ubaprec'][0])
    print('recall: %.5f' % rmetrics_dict['ubarec'][0])

    print('\nRegression Error Metrics')
    print('Average of test output:',  str(np.mean(y_test)))
    print('R2: %.2f' % r2_Score)
    print('Adj-R2: %.2f' % adjusted_r2)
    print('RMSE: %.2f' % rmse_score)
    print('MSE: %.2f' % mse_score)
    print('MAE: %.2f' % mae_score)
    print('MAPE: %.2f' % mape_score)
    print('Accuracy: %.2f' % accuracy_score)
    print('AIC:', aic)
    print('BIC:',  bic)
    print('Normalized Mutal Information:', nmi)

    print('\nCorrelations')
    print('Pearson: %.2f' % pearson_corr)
    print('Spearman: %.2f' % spearman_corr)
    print('Distance: %.2f' % distance_corr)
    scores_dict["Average"] = str(np.mean(y_test))
    scores_dict["F1"] = rmetrics_dict['ubaF1'][0]
    scores_dict["F2"] = rmetrics_dict['ubaF2'][0]
    scores_dict["F05"] = rmetrics_dict['ubaF05'][0]
    scores_dict["Precision"] = rmetrics_dict['ubaprec'][0]
    scores_dict["Recall"] = rmetrics_dict['ubarec'][0]
    scores_dict["R2"] = r2_Score
    scores_dict["Adjusted R2"] = adjusted_r2
    scores_dict["RMSE"] = rmse_score
    scores_dict["MSE"] = mse_score
    scores_dict["MAE"] = mae_score
    scores_dict["MAPE"] = mape_score
    scores_dict["Accuracy"] = accuracy_score
    scores_dict["Pearson C.C."] = pearson_corr
    scores_dict["Spearman C.C."] = spearman_corr
    scores_dict["Spatial Distance"] = distance_corr
    scores_dict["NMI"] = nmi
    scores_dict["AIC"] = aic
    scores_dict["BIC"] = bic
    scores_dict["Data Size"] = len(y_test)
    return mape_score,distance_corr,spearman_corr,pearson_corr,mae_score,mse_score,rmse_score,adjusted_r2,r2_Score,rmetrics_dict['ubaF1'][0],rmetrics_dict['ubaF2'][0],rmetrics_dict['ubaF05'][0],rmetrics_dict['ubaprec'][0],rmetrics_dict['ubarec'][0], accuracy_score, aic, bic, nmi
  
def write_to_txt(filename, content):
  text_file = open(output_path + filename, "w")
  text_file.write(content)
  text_file.close()
  
def plot_actual_vs_predicted(df, predicted_variable):
  plt.plot(list(range(1, len(df) + 1)), df[y_test_name], color='b', label='actual')
  plt.plot(list(range(1, len(df) + 1)), df[predicted_variable], color='r', label='predicted')
  plt.legend(loc='best')
  plt.suptitle('actual vs. predicted')
  plt.savefig(output_path + 'actual_vs_predicted')
  plt.close()
    
def plot_actual_vs_predicted_scatter_bisector(df, predicted_variable):
  fig, ax = plt.subplots()
  ax.scatter(df[y_test_name], df[predicted_variable], c='black')
  lims = [
      np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
      np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
  ]
  ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
  ax.set_aspect('equal')
  ax.set_xlim(lims)
  ax.set_ylim(lims)
  plt.suptitle('actual vs. predicted forecasts')
  plt.savefig(output_path + 'actual_vs_predicted_scatter_plot')
  plt.close()

def plot_target_variable(df, df_resampled, output_column, output_folder, fig_name):
    y = df[output_column]
    y_resamp = df_resampled[output_column]
    plt.plot(list(range(len(y))), sorted(y), label = "original")
    plt.plot(list(range(len(y_resamp))), sorted(y_resamp), label = "resampled")
    plt.xlabel('Index')
    plt.ylabel(target_variable)
    plt.legend()
    plt.savefig(output_folder + fig_name)
    plt.close()

## Generate lags for all input features, re-generate even if some exist so that order will not be shuffled after nan dropping
def generate_lags_for(df, column, lags_count):
        for i in range(lags_count):
            lag_name = column + "-" + str(i + 1)
            df[lag_name] = df[column].shift(i + 1)
        return df

def generate_lags(df, lagsForColumns):
    '''This function generates the lags for the list of columns'''
    for k in range(len(lagsForColumns)):
        col = lagsForColumns[k]
        if col in df.columns:
            df = generate_lags_for(df, col, 5)
    return df


def write_dict_to_json(path, content):
    with open(path + '.json', 'w') as file:
        file.write(json.dumps(content, ensure_ascii=False))

def write_dict_to_txt(path, content):
    with open(path + '.txt', 'w') as file:
        file.write(content, ensure_ascii=False)


def split_train_test_valid(df, TRAIN_RATIO, TEST_RATIO):
    X_train = pd.DataFrame()
    X_test = pd.DataFrame()
    X_valid = pd.DataFrame()
    Y_train = pd.DataFrame()
    Y_test = pd.DataFrame()
    Y_valid = pd.DataFrame()
    unique_sites = df["Site Id"].unique()
    print("Number of sites:", len(unique_sites))
    for site in unique_sites:
        df_site = df[df["Site Id"] == site]
#         X = df_site.drop([output_column], axis = 1)
#         Y = df_site[output_column]
        X = df_site
        train_index = int(X.shape[0] * TRAIN_RATIO)
        test_index = int(X.shape[0] * (TRAIN_RATIO + TEST_RATIO))
        X_train = X_train.append(X[:train_index], ignore_index = True)
        X_test = X_test.append(X[train_index:test_index], ignore_index = True)
        X_valid = X_valid.append(X[test_index:], ignore_index = True)
        
        Y_train = Y_train.append(X[:train_index], ignore_index = False)
        Y_test = Y_test.append(X[train_index:test_index], ignore_index = False)
        Y_valid = Y_valid.append(X[test_index:], ignore_index = False)
    
    Y_train = Y_train[[output_column]]
    Y_test = Y_test[[output_column]]
    Y_valid = Y_valid[[output_column]]
    
    X_train = X_train.drop([output_column], axis = 1) 
    X_test = X_test.drop([output_column], axis = 1) 
    X_valid = X_valid.drop([output_column], axis = 1) 
    
    return (X_train, Y_train, X_test, Y_test, X_valid, Y_valid)


def split_train_test_valid_date(df, month):
    X_train = pd.DataFrame()
    X_test = pd.DataFrame()
    Y_train = pd.DataFrame()
    Y_test = pd.DataFrame()
    unique_sites = df["Site Id"].unique()
    print("Number of sites:", len(unique_sites))
    for site in unique_sites:
        df_site = df[df["Site Id"] == site]
        X = df_site
        X_train = X_train.append(df_site[df_site['Month']< month+1])
        X_test = X_test.append(df_site[df_site['Month']> month])

    Y_train[output_column] = X_train[output_column]
    Y_test[output_column] = X_test[output_column]
   
    X_train = X_train.drop([output_column], axis = 1)
    X_test = X_test.drop([output_column], axis = 1)

    return X_train, Y_train, X_test, Y_test


def error_metrics(y_test, y_pred, nb_columns, nb_param):
    n = len(y_test)
    #convert to float for it to work for AIC and BIC data should all be of the same format i.e float not mix of types
    y_test_f =  [float(item) for item in y_test.values]
    y_predict_f =  [float(item) for item in y_pred]
    test_scores = {}
    r2_Score = r2_score(y_test, y_pred)  # r-squared
    adjusted_r2 = 1 - ((1 - r2_Score) * (n - 1)) / (n - nb_columns - 1)  # adjusted r-squared
    rmse_score = np.sqrt(mean_squared_error(y_test, y_pred))  # RMSE
    mse_score = mean_squared_error(y_test_f, y_predict_f)  # MSE
    mae_score = mean_absolute_error(y_test, y_pred)  # MAE
    mape_score = np.asarray(np.abs(( np.array(y_test) - np.array(y_pred)) / np.array(y_test)), dtype=np.float64).mean() * 100  # MAPE
    re = (mse_score / np.mean(y_pred)) * 100
    accuracy = 100 - np.mean(mape_score)
    spearman_corr, _ = spearmanr(y_test, y_pred)
    r2 = str(round(r2_Score, 2))
    adjusted_r2 = str(round(adjusted_r2, 2))
    mae = str(round(mae_score, 2))
    mse = str(round(mse_score, 2))
    rmse = str(round(rmse_score, 2))
    re = str(round(re, 2))
    spearman = str(round(spearman_corr, 2))
    mape = str(round(mape_score, 2)) + "%"
    accuracy = str(round(accuracy, 2)) + "%"

    def calculate_aic(n, mse, num_params):
        aic = n * log(mse) + 2 * num_params
        return aic

    def calculate_bic(n, mse, num_params):
        bic = n * log(mse) + num_params * log(n)
        return bic

    aic = str(round(calculate_aic(len(y_test), mse_score, nb_param), 2))
    bic = str(round(calculate_bic(len(y_test), mse_score, nb_param), 2))

    # y_test_arr = np.array(y_test)
    nmi = 1
     # normalized_mutual_info_score(y_test_arr, y_pred)

    test_scores["average output"] =  str(y_test.mean())
    test_scores["R2"] = r2
    test_scores["Adjusted R2"] = adjusted_r2
    test_scores["MAE"] = mae
    test_scores["MSE"] = mse
    test_scores["RMSE"] = rmse
    test_scores["Relative Error"] = re
    test_scores["Spearman"] = spearman
    test_scores["MAPE"] = mape
    test_scores["Accuracy"] = accuracy
    test_scores["Information Mutal Normalised"] = nmi
    test_scores["AIC"] = aic
    test_scores["BIC"] = bic
    return test_scores


def binary_encode_column(df, columnToEncode):
    encoder = ce.BinaryEncoder(cols=[columnToEncode])
    df_encoder = encoder.fit_transform(df[columnToEncode])
    df = pd.concat([df, df_encoder], axis=1)
    return df

def export_scores(file_name, scores, columnName):
        if not os.path.exists(file_name):
            df = pd.DataFrame(list())
            df.to_csv(file_name, index=False)
        else:
            df = pd.read_csv(file_name, delimiter=',')
        df["Error Metrics"] = scores.keys()
        df[columnName] = scores.values()
        df.to_csv(file_name, index=False)
        return df

########################################################################################################################
                                            #Establish a connection to R library
########################################################################################################################
rpy2.robjects.numpy2ri.activate()
runit = robjects.r
# runit['source'](user_path + 'smogn.R')
runit['source']('smogn.R')

########################################################################################################################
                                            #Read and Preprocess Dataset
########################################################################################################################


## Get Data
# df_experiment = pd.read_csv(experiment_path, "Daily")
# df_experiment

#read csv
df = pd.read_csv(input_path, delimiter=',')

#drop NaN values
# df.dropna(inplace=True)

# filter sites if ameri or euro
# df = df[df["Site Id"].str.startswith('US-')]
# df = df[~df["Site Id"].str.startswith('US-')]


#drop desired columns, rename, and drop the nans

#base columns
# columnsToDrop = ['Date','Year','Month','Day',
#                  'Climate', 'Vegetation', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'Climate_1', 'Climate_2', 'Climate_3',
#                  'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
#                  'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
#                  'Longitude_5', 'Longitude_6',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5', 'C_BOWENS',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr',
#                  'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
#                  'Elevation(m)_5', 'Elevation(m)_6',
#                  'ETo', 'EToF', 'ETr', 'ETrF', 'Site Id_1', 'Site Id_2', 'Site Id_3', 'Site Id_4', 'Site Id_5',
# 'Site Id_6', 'Month_1', 'Month_2', 'Month_3', 'Month_4','Vegetation_1', 'Vegetation_2','Vegetation_3', 'SW_IN']

#sw in columns
# columnsToDrop = ['Date','Year','Month','Day',
#                  'Climate', 'Vegetation', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'Climate_1', 'Climate_2', 'Climate_3',
#                  'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
#                  'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
#                  'Longitude_5', 'Longitude_6',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5', 'C_BOWENS',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr',
#                  'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
#                  'Elevation(m)_5', 'Elevation(m)_6',
#                  'ETo', 'EToF', 'ETr', 'ETrF', 'Site Id_1', 'Site Id_2', 'Site Id_3', 'Site Id_4', 'Site Id_5',
# 'Site Id_6', 'Month_1', 'Month_2', 'Month_3', 'Month_4','Vegetation_1', 'Vegetation_2','Vegetation_3']


#encoded columns
# columnsToDrop = ['Date','Year','Month','Day',
#                  'Climate', 'Vegetation', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'Climate_1', 'Climate_2', 'Climate_3',
#                  'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
#                  'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
#                  'Longitude_5', 'Longitude_6',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5', 'C_BOWENS',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr',
#                  'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
#                  'Elevation(m)_5', 'Elevation(m)_6',
#                  'ETo', 'EToF', 'ETr', 'ETrF', 'SW_IN', 'RH_avg', 'WS_avg', 'TA_avg', 'Cluster']


#All columns
# columnsToDrop = ['Date','Year','Month','Day',
#                  'Climate', 'Vegetation', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'Climate_1', 'Climate_2', 'Climate_3',
#                  'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
#                  'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
#                  'Longitude_5', 'Longitude_6',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5', 'C_BOWENS',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr',
#                  'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
#                  'Elevation(m)_5', 'Elevation(m)_6',
#                  'ETo', 'EToF', 'ETr', 'ETrF']

#### Library columns
#base columns
# columnsToDrop = ['Date','Year','Month','Day','Latitude','Longitude',
#                  'Climate','Vegetation', 'G','G-1','G-2','G-3','G-4','G-5',
#                  'H','H_bowen_corr','H_bowen_corr-1','H_bowen_corr-2','H_bowen_corr-3',
#                  'H_bowen_corr-4','H_bowen_corr-5', 'H_ebr_corr','H_ebr_corr-1','H_ebr_corr-2',
#                  'H_ebr_corr-3','H_ebr_corr-4','H_ebr_corr-5','LE_ebr_corr','LE_ebr_corr(mm)',
#                  'ET_bowen','ET_bowen_corr','ET_bowen_corr(mm)','ET_ebr','ET_ebr_corr',
#                  'ET_ebr_corr(mm)' ,'NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4',
#                  'NETRAD-5','LE','LE_bowen_corr', 'ETo','EToF_bowen','EToF_ebr',
#                  'ETr','ETrF_bowen','ETrF_ebr', 'Climate_1',
#                  'Climate_2','Climate_3', 'Latitude_1',
#                  'Latitude_2','Latitude_3','Latitude_4','Latitude_5','Latitude_6', 'Longitude_1',
#                  'Longitude_2','Longitude_3','Longitude_4','Longitude_5','Longitude_6',
#                  'Elevation(m)_1','Elevation(m)_2','Elevation(m)_3','Elevation(m)_4',
#                  'Elevation(m)_5','Elevation(m)_6', 'NETRAD', 'Site Id_1', 'Site Id_2', 'Site Id_3', 'Site Id_4', 'Site Id_5',
# 'Site Id_6', 'Month_1', 'Month_2', 'Month_3', 'Month_4','Vegetation_1', 'Vegetation_2','Vegetation_3', 'SW_IN']

#encoded columns
# columnsToDrop = ['Date','Year','Day','Latitude','Longitude',
#                  'Climate', 'G','G-1','G-2','G-3','G-4','G-5',
#                  'H','H_bowen_corr','H_bowen_corr-1','H_bowen_corr-2','H_bowen_corr-3',
#                  'H_bowen_corr-4','H_bowen_corr-5', 'H_ebr_corr','H_ebr_corr-1','H_ebr_corr-2',
#                  'H_ebr_corr-3','H_ebr_corr-4','H_ebr_corr-5','LE_ebr_corr','LE_ebr_corr(mm)',
#                  'ET_bowen','ET_bowen_corr','ET_bowen_corr(mm)','ET_ebr','ET_ebr_corr',
#                  'ET_ebr_corr(mm)' ,'NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4',
#                  'NETRAD-5','LE','LE_bowen_corr', 'ETo','EToF_bowen','EToF_ebr',
#                  'ETr','ETrF_bowen','ETrF_ebr', 'Climate_1',
#                  'Climate_2','Climate_3', 'Latitude_1',
#                  'Latitude_2','Latitude_3','Latitude_4','Latitude_5','Latitude_6', 'Longitude_1',
#                  'Longitude_2','Longitude_3','Longitude_4','Longitude_5','Longitude_6',
#                  'Elevation(m)_1','Elevation(m)_2','Elevation(m)_3','Elevation(m)_4',
#                  'Elevation(m)_5','Elevation(m)_6', 'NETRAD', 'SW_IN',
#                  'Site Id_1', 'Site Id_2', 'Site Id_3', 'Site Id_4', 'Site Id_5',
#        'Site Id_6', 'Month_1', 'Month_2', 'Month_3', 'Month_4', 'Vegetation_1',
#        'Vegetation_2', 'Vegetation_3']

#swin columns
# columnsToDrop = ['Date','Year','Month','Day','Latitude','Longitude',
#                  'Climate','Vegetation', 'G','G-1','G-2','G-3','G-4','G-5',
#                  'H','H_bowen_corr','H_bowen_corr-1','H_bowen_corr-2','H_bowen_corr-3',
#                  'H_bowen_corr-4','H_bowen_corr-5', 'H_ebr_corr','H_ebr_corr-1','H_ebr_corr-2',
#                  'H_ebr_corr-3','H_ebr_corr-4','H_ebr_corr-5','LE_ebr_corr','LE_ebr_corr(mm)',
#                  'ET_bowen','ET_bowen_corr','ET_bowen_corr(mm)','ET_ebr','ET_ebr_corr',
#                  'ET_ebr_corr(mm)' ,'NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4',
#                  'NETRAD-5','LE','LE_bowen_corr', 'ETo','EToF_bowen','EToF_ebr',
#                  'ETr','ETrF_bowen','ETrF_ebr', 'Climate_1',
#                  'Climate_2','Climate_3', 'Latitude_1',
#                  'Latitude_2','Latitude_3','Latitude_4','Latitude_5','Latitude_6', 'Longitude_1',
#                  'Longitude_2','Longitude_3','Longitude_4','Longitude_5','Longitude_6',
#                  'Elevation(m)_1','Elevation(m)_2','Elevation(m)_3','Elevation(m)_4',
#                  'Elevation(m)_5','Elevation(m)_6', 'NETRAD', 'Site Id_1', 'Site Id_2', 'Site Id_3', 'Site Id_4', 'Site Id_5',
# 'Site Id_6', 'Month_1', 'Month_2', 'Month_3', 'Month_4','Vegetation_1', 'Vegetation_2','Vegetation_3']



#all columns
# columnsToDrop = ['Date','Year','Month','Day','Latitude','Longitude',
#                  'Climate','Vegetation', 'G','G-1','G-2','G-3','G-4','G-5',
#                  'H','H_bowen_corr','H_bowen_corr-1','H_bowen_corr-2','H_bowen_corr-3',
#                  'H_bowen_corr-4','H_bowen_corr-5', 'H_ebr_corr','H_ebr_corr-1','H_ebr_corr-2',
#                  'H_ebr_corr-3','H_ebr_corr-4','H_ebr_corr-5','LE_ebr_corr','LE_ebr_corr(mm)',
#                  'ET_bowen','ET_bowen_corr','ET_bowen_corr(mm)','ET_ebr','ET_ebr_corr',
#                  'ET_ebr_corr(mm)' ,'NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4',
#                  'NETRAD-5','LE','LE_bowen_corr', 'ETo','EToF_bowen','EToF_ebr',
#                  'ETr','ETrF_bowen','ETrF_ebr', 'Climate_1',
#                  'Climate_2','Climate_3', 'Latitude_1',
#                  'Latitude_2','Latitude_3','Latitude_4','Latitude_5','Latitude_6', 'Longitude_1',
#                  'Longitude_2','Longitude_3','Longitude_4','Longitude_5','Longitude_6',
#                  'Elevation(m)_1','Elevation(m)_2','Elevation(m)_3','Elevation(m)_4',
#                  'Elevation(m)_5','Elevation(m)_6', 'NETRAD']

##joint columns
# columnsToDrop = ['Year','Day', 'Date',
#                  'Climate', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr', 'Cloud',
#                  'Tier', 'EEflux EToF', 'EEflux ETrF',
#                  'H_ebr_corr','H_ebr_corr-1', 'H_ebr_corr-2', 'H_ebr_corr-3', 'H_ebr_corr-4',
#                  'H_ebr_corr-5', 'LE_bowen_corr(mm)', 'LE_ebr_corr',
#                  'LE_ebr_corr(mm)', 'ET_bowen_corr', 'ET_ebr_corr',
#                  'ET_ebr_corr(mm)', 'ETr', 'ET_bowen_corr(mm)', 'SW_IN',
#                  'EToF_bowen', 'EToF_ebr', 'ETrF_bowen',
#                  'ETrF_ebr', 'EEflux ETr']

# columnsToDrop = ['Date','Year','Month','Day','Latitude','Longitude',
#                  'Climate','Vegetation', 'G','G-1','G-2','G-3','G-4','G-5',
#                  'H','H_bowen_corr','H_bowen_corr-1','H_bowen_corr-2','H_bowen_corr-3',
#                  'H_bowen_corr-4','H_bowen_corr-5', 'H_ebr_corr','H_ebr_corr-1','H_ebr_corr-2',
#                  'H_ebr_corr-3','H_ebr_corr-4','H_ebr_corr-5','LE_ebr_corr','LE_ebr_corr(mm)',
#                  'ET_bowen','ET_bowen_corr','LE_bowen_corr(mm)','ET_ebr','ET_ebr_corr',
#                  'ET_ebr_corr(mm)' ,'NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4',
#                  'NETRAD-5','LE','LE_bowen_corr','EToF_bowen','EToF_ebr',
#                  'ETr','ETrF_bowen','ETrF_ebr', 'Climate_1',
#                  'Climate_2','Climate_3', 'Latitude_1',
#                  'Latitude_2','Latitude_3','Latitude_4','Latitude_5','Latitude_6', 'Longitude_1',
#                  'Longitude_2','Longitude_3','Longitude_4','Longitude_5','Longitude_6',
#                  'Elevation(m)_1','Elevation(m)_2','Elevation(m)_3','Elevation(m)_4',
#                  'Elevation(m)_5','Elevation(m)_6', 'NETRAD', 'SW_IN']

#manual joint encoded
# columnsToDrop = ['Date','Year','Day',
#                  'Climate', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr', 'ETr', 'ETo', 'EEflux ET',
#                  'EToF', 'ETrF', 'Cloud', 'Image Id',
#                  'Tier', 'EEflux ETr', 'EEflux ETo', 'EEflux EToF', 'EEflux ETrF', 'EEflux ET', 'SW_IN']

#library joint encoded
# columnsToDrop = ['Year','Day', 'Date',
#                  'Climate', 'Latitude', 'Longitude',
#                  'G','G-1','G-2','G-3','G-4','G-5',
#                  'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
#                  'H_bowen_corr-5',
#                  'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
#                  'LE', 'LE_bowen_corr', 'Cloud',
#                  'Tier', 'EEflux EToF', 'EEflux ETrF',
#                  'H_ebr_corr','H_ebr_corr-1', 'H_ebr_corr-2', 'H_ebr_corr-3', 'H_ebr_corr-4',
#                  'H_ebr_corr-5', 'LE_ebr_corr',
#                  'LE_ebr_corr(mm)', 'ET_bowen_corr', 'ET_ebr_corr',
#                  'ET_ebr_corr(mm)', 'ETr', 'ET_bowen_corr(mm)', 'SW_IN',
#                  'EToF_bowen', 'EToF_ebr', 'ETrF_bowen', 'EEflux ET',
#                  'ETrF_ebr', 'EEflux ET', 'EEflux ETr', 'ET_bowen_corr(mm)', 'EEflux ETo', 'ETo']
                
columnsToDrop = ['Year', 'Month', 'Day', 'Site Id_1', 'Site Id_2', 'Site Id_3',
                        'Site Id_4', 'Site Id_5', 'Site Id_6','Vegetation', 'Latitude', 'Longitude',
                        'G','G-1','G-2','G-3','G-4','G-5',
                        'Climate_1', 'Climate_2', 'Climate_3',
                        'Latitude_1','Latitude_2', 'Latitude_3', 'Latitude_4', 'Latitude_5',
                        'Latitude_6','Longitude_1', 'Longitude_2', 'Longitude_3', 'Longitude_4',
                        'Longitude_5', 'Longitude_6',
                        'H', 'H_bowen_corr', 'H_bowen_corr-1', 'H_bowen_corr-2', 'H_bowen_corr-3', 'H_bowen_corr-4',
                        'H_bowen_corr-5', 'C_BOWENS',
                        'NETRAD','NETRAD-1','NETRAD-2','NETRAD-3','NETRAD-4','NETRAD-5',
                        'LE', 'LE_bowen_corr',
                        'Elevation(m)_1','Elevation(m)_2', 'Elevation(m)_3', 'Elevation(m)_4',
                        'Elevation(m)_5', 'Elevation(m)_6',
                        'ETo', 'EToF', 'ETr', 'ETrF', 'ETo', 'SW_IN']
df = df.drop(columnsToDrop, axis = 1)

df.dropna(inplace=True)

model_name = "all_manual_experiment"

#specify the output column
output_column = "LE_bowen_corr(mm)"
df = df[df[output_column].between(1, 15)]

#specify name of target variable and name of predicted target variable
y_test_name = output_column
y_test_pred_name = 'LE_bowen_corr_mm(pred)'

#generate lags for columns
lagsForColumns = ["SW_IN", "WS", "RH", "TA", "EEflux LST", "EEflux Albedo", "EEflux NDVI"]
df = generate_lags(df, lagsForColumns)

#Generate binary encoding only for joint experiments
#Binary Encode Site Id
df = binary_encode_column(df, "Site Id")
df.drop(columns=['Site Id_0'], inplace=True)

#FS2
# columnsToTake = ['Date', 'Site Id', 'TA', 'TA-1', 'TA-2', 'TA-3', 'TA-4', 'TA-5',
#                          'EEflux LST','EEflux LST-1', 'EEflux LST-2', 'EEflux LST-3',
#                          'EEflux LST-4', 'EEflux LST-5', 'WS', 'WS-1', 'WS-2',
#                          'RH', 'RH-1', 'RH-2', 'RH-3',
#                        'EEflux NDVI', 'EEflux NDVI-1', 'EEflux NDVI-2',
#                          'EEflux Albedo', 'EEflux Albedo-1',
#                        'EEflux Albedo-2', 'LE_bowen_corr(mm)', 'Site Id_1', 'Site Id_2',
#                        'Site Id_3', 'Site Id_4', 'Site Id_5', 'Month_1', 'Month_2', 'Month_3',
#                        'Month_4', 'Vegetation_1', 'Vegetation_2', 'Vegetation_3', 'Climate']

#FS1
# columnsToTake = ['Date', 'Site Id', 'TA', 'TA-1', 'TA-2', 'TA-3', 'TA-4', 'TA-5',
#                          'LE_bowen_corr(mm)', 'Site Id_1', 'Site Id_2',
#                        'Site Id_3', 'Site Id_4', 'Site Id_5', 'Month_1', 'Month_2', 'Month_3',
#                        'Month_4', 'Vegetation_1', 'Vegetation_2', 'Vegetation_3', 'Climate']

#FS3
columnsToTake = ['Date', 'Site Id', 'TA', 'TA-1', 'TA-2', 'TA-3', 'TA-4', 'TA-5',
                         'EEflux LST','EEflux LST-1', 'EEflux LST-2', 'EEflux LST-3', 'Climate',
                         'EEflux LST-4', 'EEflux LST-5','RH', 'RH-1', 'RH-2', 'RH-3', 'LE_bowen_corr(mm)']
df = df[columnsToTake]

df = df[df["Climate"] == "Dsa"]
df.drop(columns=['Climate'], inplace=True)
print(df.columns)
print(df.shape)

#drop nan for the first 5 rows of the generated lags only 5 rows will be removed in here
df.isnull().mean() * 10
df.dropna(inplace=True)
print(df.shape)

print("checking null values in the whole dataset")
print(df.isnull().values.any())

df = df.sample(frac=1).reset_index(drop=True)

num_features = df.shape[1]
print(num_features)


########################################################################################################################
                                            #Train and Test
########################################################################################################################

TRAIN_RATIO = 0.4
TEST_RATIO = 0.3
    
X_train, Y_train, X_test, Y_test, X_valid, Y_valid = split_train_test_valid(df, TRAIN_RATIO, TEST_RATIO)
print("X_train shape:", X_train.shape, "X_valid shape:", X_valid.shape, "X_test shape", X_test.shape)
print("Y_train shape:", Y_train.shape, "Y_valid shape:", Y_valid.shape, "Y_test shape", Y_test.shape)
# X_train_tmp, Y_train_tmp, X_test_tmp, Y_test_tmp = split_train_test_valid_date(df, 7)
# print("Date split - X_train shape:", X_train_tmp.shape, "Y_train shape:", Y_train_tmp.shape)
# print("Date split - X_test shape:", X_test_tmp.shape, "Y_test shape:", Y_test_tmp.shape)

date_list = X_test["Date"]
site_list = X_test["Site Id"]

train_date_list = X_train["Date"]
train_site_list = X_train["Site Id"]

columnsToDrop = ["Site Id", "Date"]
X_train.drop(columnsToDrop, axis = 1, inplace=True)
X_test.drop(columnsToDrop, axis = 1, inplace=True)
X_valid.drop(columnsToDrop, axis = 1, inplace=True)

# X_train, X_valid, Y_train, Y_valid = train_test_split(X_train_tmp, Y_train_tmp, test_size=0.2, random_state=42)
# print("X_train shape:", X_train.shape, "X_valid shape:", X_valid.shape)
# print("Y_train shape:", Y_train.shape, "Y_valid shape:", Y_valid.shape)

X_train.dropna(inplace=True)
Y_train.dropna(inplace=True)
X_test.dropna(inplace=True)
Y_test.dropna(inplace=True)
X_valid.dropna(inplace=True)

scaler = MinMaxScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)
X_valid_scaled = scaler.transform(X_valid)

n_input = X_train.shape[1]
n_classes = 1
print("num of input:", n_input, "num of classes:", n_classes)


print("Xtrain scaled is nan: ", np.isnan(X_train_scaled).any())
print("Xtest scaled is nan: ", np.isnan(X_test_scaled).any())
print("Xvalid scaled is nan: ", np.isnan(X_valid_scaled).any())


########################################################################################################################
                                            #Prepare Experiment Details Json
########################################################################################################################


# current_experiment = df_experiment.iloc[10, :]
# experiment_info = current_experiment.to_dict()
# experiment_info["Input Features"] = list(X_train.columns)
# experiment_info["Output Feature"] = str(Y_train.columns[0])
# experiment_info["Train Size"] = str(X_train.shape)
# experiment_info["Validation Size"] = str(X_valid.shape)
# experiment_info["Test Size"] = str(X_test.shape)

# sorted_dict = OrderedDict(sorted(experiment_info.items(), key=lambda t: t[0]))
# write_dict_to_json(output_path + "experiment_info", sorted_dict)



########################################################################################################################
                                        #Grid Search + CV
########################################################################################################################

##Create model and start training

n_input = X_train_scaled.shape[1]
n_classes = 1
print("num of input:", n_input, "num of classes:", n_classes)


def create_model(first_neuron=30,
                 activation='relu',
                 init='uniform',
                 dropout_rate = 0.0,
                 dense_layer_sizes = 2,
                 optimizer='Adam',
                 loss='mean_absolute_percentage_error'):
    # Create model
    model = tensorflow.keras.Sequential()
    model.add(Dense(first_neuron, input_dim=n_input, kernel_initializer=init, activation=activation))
    model.add(BatchNormalization())
    model.add(Dropout(dropout_rate))
    for layer_size in range(dense_layer_sizes):
        model.add(Dense(first_neuron, activation=activation))
        model.add(Dropout(dropout_rate))
    model.add(Dense(1, activation="linear"))
    # Compile model
    model.compile(optimizer=optimizer, loss=loss, metrics=['mse', 'mae', 'accuracy', 'mape'])
    return model

create_model().summary


model = KerasRegressor(build_fn=create_model)

# Prepare the Grid
param_grid = {
              'first_neuron':[64],
              'activation' : ['softmax'],
              'init': ['uniform'],
              'dropout_rate' : [0.4],
              'dense_layer_sizes' : [2],
              'optimizer' : ['Adam'],
              'loss': ['mse'],
              'epochs':[500],
              'batch_size':[64]
             }

best_params = param_grid


start_time = time.time()
print("Training Start time:", start_time)

#fitting regressor
model = create_model(param_grid["first_neuron"][0],
                    param_grid["activation"][0],
                    param_grid["init"][0],
                    param_grid["dropout_rate"][0],
                    param_grid["dense_layer_sizes"][0],
                    param_grid["optimizer"][0],
                    param_grid["loss"][0])

es = EarlyStopping(monitor='val_loss', mode='min', verbose=0, patience=100)
mc = ModelCheckpoint(output_path + model_name, monitor='val_mape', mode='min', verbose=0, save_best_only=True)
history = model.fit(X_train_scaled,
                    Y_train,
                    validation_data=(X_valid_scaled, Y_valid),
                    epochs=param_grid["epochs"][0],
                   verbose=0,
                   callbacks=[es, mc])

training_time = (time.time() - start_time)
print("Training time in seconds: %.5f seconds" % training_time)

########################################################################################################################
                                        #Predict Model
########################################################################################################################

#Save start testing time
start_time = time.time()
print("Testing Start time:", start_time)

Y_predict = model.predict(X_test_scaled)
trainable_count = np.sum([K.count_params(w) for w in model.trainable_weights])
print("num parameters:", trainable_count)
num_params = trainable_count

#Export testing scores
y_test_df = pd.DataFrame(Y_test)
X_test[y_test_name] = y_test_df.values

y_predict_df = pd.DataFrame(Y_predict)
len(y_predict_df)
X_test[y_test_pred_name] = y_predict_df

mape_score,distance_corr,spearman_corr,pearson_corr,mae_score,mse_score,rmse_score,adjusted_r2,r2_Score,f1,f2,f5,prec,recall, acc, aic, bic, nmi  = evaluate(X_test, actual=y_test_name, predicted=y_test_pred_name, num_params=num_params)

#Save end testing time
testing_time = (time.time() - start_time)
print("Testing time in seconds: %.5f seconds" % testing_time)
        
    
scores_dict["Training Time (seconds)"] = training_time
scores_dict["Testing Time (seconds)"] = testing_time

print("Test Scores are", scores_dict)
testing_path = output_path + "scores_results"
export_scores(testing_path, scores_dict, 'Testing')

#Export validation scores
## Evaluate scores for train, test and validation
loss, train_mse, train_mae, train_acc, train_mape = model.evaluate(X_train_scaled, Y_train, verbose=2)
loss, val_mse, val_mae, val_acc, val_mape = model.evaluate(X_valid_scaled, Y_valid, verbose=2)
loss, test_mse, test_mae, test_acc, test_mape = model.evaluate(X_test_scaled, Y_test, verbose=2)

print("train mse:", train_mse, "validation mse:", val_mse, "test mse:", test_mse)
print("train mae:", train_mae, "validation mae:", val_mae, "test mae:", test_mae)
print("train mape:", train_mape, "validation mape:", val_mape, "test mape:", test_mape)

scores_dict = OrderedDict()
scores_dict["Average"] = "-"
scores_dict["F1"] = "-"
scores_dict["F2"] = "-"
scores_dict["F05"] = "-"
scores_dict["Precision"] = "-"
scores_dict["Recall"] = "-"
scores_dict["R2"] = "-"
scores_dict["Adjusted R2"] = "-"
scores_dict["RMSE"] = "-"
scores_dict["MSE"] = str(round(val_mse, 2))
scores_dict["MAE"] = str(round(val_mae, 2))
scores_dict["MAPE"] = str(round(val_mape, 2))
scores_dict["Accuracy"] = str(round(100 - np.mean(val_mape), 2))
scores_dict["Pearson C.C."] = "-"
scores_dict["Spearman C.C."] = "-"
scores_dict["Spatial Distance"] = "-"
scores_dict["NMI"] = "-"
scores_dict["AIC"] = "-"
scores_dict["BIC"] = "-"
scores_dict["Data Size"] = "-"
scores_dict["Training Time (seconds)"] = training_time
scores_dict["Testing Time (seconds)"] = testing_time
export_scores(testing_path, scores_dict, 'Validation')


##Export data sets and plot
y_train_df = pd.DataFrame(Y_train)
X_train[y_test_name] = y_train_df.values
X_train['Date'] = train_date_list
X_train['Site Id'] = train_site_list
X_train.head()
X_train.to_csv(output_path + 'train_dataset.csv')

X_test[y_test_name] = y_test_df.values
X_test[y_test_pred_name] = y_predict_df
X_test['Date'] = date_list
X_test['Site Id'] = site_list
X_test.head()
X_test.to_csv(output_path + 'test_dataset.csv')

print("checking null values in test")
print(X_test.isnull().values.any())

#plotting actual vs predicted
plot_actual_vs_predicted(X_test, y_test_pred_name)
plot_actual_vs_predicted_scatter_bisector(X_test, y_test_pred_name)


########################################################################################################################
                                        #Learing Curves and Model Architecture
########################################################################################################################


model.save_weights(output_path + "model_weights.h5")
model_json = model.to_json()
with open(output_path + "model_architecture.json", "w") as json_file:
    json_file.write(model_json)

## Plot learning curve
plt.plot(history.history['mse'])
plt.plot(history.history['val_mse'])
plt.title('MSE Loss')
plt.ylabel('mse')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.savefig(output_path + 'learning_curve_train_val_mse')
plt.close()

plt.plot(history.history['mae'])
plt.plot(history.history['val_mae'])
plt.title('MAE Loss')
plt.ylabel('mae')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.savefig(output_path +  'learning_curve_train_val_mae')
plt.close()

plt.plot(history.history['mape'])
plt.plot(history.history['val_mape'])
plt.title('MAPE Loss')
plt.ylabel('mape')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.savefig(output_path +  'learning_curve_train_val_mape')
plt.close()

write_dict_to_json(output_path + "best_params", best_params)

