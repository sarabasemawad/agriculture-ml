import numpy as np
import pandas as pd
from sklearn.preprocessing import RobustScaler, OneHotEncoder, MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
from dataset_preparation import DataPreparation

output_column = "LE_bowen_corr(mm)"

class TaskGenerator(object):
    def new_task(self):
        dataPreparation = DataPreparation()
        df = dataPreparation.read_data()
#         df = dataPreparation.read_all_climates()
        print("df shape:", df.shape)
        return Task(df)

class Task(object):
    def __init__(self, df):
        self.df = df

    def save_dates_sites(self, df):
        #Save date and site id for export
        dates_list = df["Date"]
        sites_list = df["Site Id"]
        return dates_list, sites_list

    def remove_date_site_columns(self, df):
         df.drop(["Date", "Site Id"], axis=1, inplace=True)
        
    def split_data_sample(self, df, k):
        self.remove_date_site_columns(df)
        df_sub = self.df.sample(n=k, replace=False)
        X = df_sub.drop([output_column], axis = 1)
        Y = df_sub[output_column]
        scaler = MinMaxScaler()
        X = scaler.fit_transform(X)
        Y = np.array(Y).reshape(-1, 1)
        return X, Y
    
    def next(self, k):
        return self.split_data_sample(self.df, k)
    
    def eval(self, test_rows):
        return self.split_data_sample(self.df, test_rows)

    def next_eval(self, rows, test_rows):
        df_train = self.df.iloc[:rows, ]
        df_test = self.df.iloc[rows:rows+test_rows, ]

        dates_list_train, sites_list_train = self.save_dates_sites(df_train)
        dates_list, sites_list = self.save_dates_sites(df_test)

        self.remove_date_site_columns(self.df)
        self.remove_date_site_columns(df_train)
        self.remove_date_site_columns(df_test)
        print("df train shape:", df_train.shape, "df test shape:", df_test.shape)
        columns = []
        columns.extend(self.df.columns)
        columns.remove(output_column)

        X_train, Y_train = self.split_features_label(df_train)
        X_test, Y_test = self.split_features_label(df_test)
        scaler = MinMaxScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)
        return X_train_scaled, Y_train, X_test_scaled, Y_test,  dates_list_train, sites_list_train, dates_list, sites_list, columns

    def split_features_label(self, df):
        X = df.drop([output_column], axis = 1)
        Y = df[output_column]
        Y = np.array(Y).reshape(-1, 1)
        return X, Y
