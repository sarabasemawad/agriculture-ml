import numpy as np
import pandas as pd
import time
import os
import matplotlib.pyplot as plt
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 

from datetime import datetime

from CustomTaskGenerator import CustomTaskGenerator
from dataset_preparation import DataPreparation
from MAML import MAMLModel
from FOMAML import FOMAMLModel
from Reptile import ReptileModel

from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.metrics.cluster import normalized_mutual_info_score
from scipy.stats.stats import pearsonr, spearmanr
from collections import OrderedDict
from sklearn.preprocessing import RobustScaler, OneHotEncoder, MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

if __name__ == "__main__":
	dataPreparation = DataPreparation()
	task_dist = CustomTaskGenerator()
	df = dataPreparation.read_data()
	climateToTest = "Cwa"
	df_test_climate = df[df["Climate"] == climateToTest]
	num = int(np.ceil(len(df)))
    #int(np.ceil(len(df) * 0.7))
    #8718
    #8729
    #int(np.ceil(9250 * 0.8))
    #int(np.ceil(10911*0.8))
	k = 1
	grad_steps = 1
    #32 # 64 #Initial is 32
	episodes = 1500
    #500
    #3000
    #3000 # number of tasks
	rows=num
	eval_rows=int(np.ceil(len(df_test_climate)))
    #2193
    #2182
    #int(np.ceil(9250 * 0.2))
    #2182
#     500
	columns=37
    #48
    #37
    #34
    #44
    #48
	n_tests = 100
	#100 #number of iterations for test
	output_path = dataPreparation.output_path
	output_column = dataPreparation.output_column
	
	average_output = 0.0
	testing_path = output_path + "test_scores.csv"

	training_time_models = {}
	testing_time_models = {}
	parameters = {}
	parameters['k'] = k
	parameters['test samples'] = eval_rows
	parameters['gradient steps'] = grad_steps
	parameters['number of tasks'] = episodes
	parameters['number of columns'] = columns
	parameters['number of test experiments'] = n_tests
	parameters['hidden 1'] = 64
	parameters['hidden 2'] = 64
	parameters['inner learning rate'] = str(1e-2)
	parameters['meta learning rate'] = str(1e-3)
	parameters['optimizer'] = 'Adam'
	dataPreparation.write_dict_to_json(output_path + "parameters", parameters)
    
	def save_dates_sites(df):
		#Save date and site id for export
		dates_list = df["Date"]
		sites_list = df["Site Id"]
		return dates_list, sites_list
    
	def remove_date_site_columns(df):
		if "Date" in df.columns:
			df.drop(["Date", "Site Id"], axis=1, inplace=True)
        
	def split_data_sample(df_sub, climate):
		remove_date_site_columns(df_sub)
		print('climate in train is:', climate)
		df_new = df_sub[df_sub["Climate"] == climate]
		df_new = df_sub.drop(["Climate"], axis=1)
# 		df_new = df_new.sample(frac=1).reset_index(drop=True)   
		X = df_new.drop([output_column], axis = 1)
		Y = df_new[output_column]
		scaler = MinMaxScaler()
		X = scaler.fit_transform(X)
		Y = np.array(Y).reshape(-1, 1)
		return X, Y
   
	def split_features_label(df):
# 		df_new = df[df["Climate"] == climateToTest]
		df_new = df.drop(["Climate"], axis=1)
		X = df_new.drop([output_column], axis = 1)
		Y = df_new[output_column]
		Y = np.array(Y).reshape(-1, 1)
		return X, Y
    
	with tf.Session() as sess:
		models = {
# 			"maml": MAMLModel(name="maml", sess=sess, rows=rows, grad_steps=grad_steps, columns=columns, eval_rows=eval_rows),
# 			"fomamlv1": MAMLModel(name="fomamlv1", sess=sess, grad_steps=grad_steps, fo=True, rows=rows, columns=columns, eval_rows=eval_rows),
# 			"fomamlv2": FOMAMLModel(name="fomamlv2", sess=sess, grad_steps=grad_steps, rows=rows, columns=columns),
			"reptile": ReptileModel(name="reptile", sess=sess, grad_steps=grad_steps, rows=rows, columns=columns, eval_rows=eval_rows),
		}
		sess.run(tf.global_variables_initializer())
		saver = tf.train.Saver()
        
		df = df.sample(frac=1).reset_index(drop=True)   
		df_train = df.iloc[:rows, ]
		df_test = df.iloc[:eval_rows, ]
		df_test = df_test[df_test["Climate"] == climateToTest]
		#Loop over episodes or number of iterations
 		#Get a new task each time       
		for i in np.arange(episodes):
			df_sub = df_train.copy()
			print('shape:', df_sub.shape, "i iteration:", str(i))
			if i == 0 and i <= 300:
				x, y = split_data_sample(df_sub, 'Dsa')
			elif i > 300 and i <= 600:
				x, y = split_data_sample(df_sub, 'Csb')
			elif i > 600 and i <= 900:
				x, y = split_data_sample(df_sub, 'Csa')
			elif i > 900 and i <= 1200:
				x, y = split_data_sample(df_sub, 'Cfa')
			elif i > 1200 and i <= 1500:
				x, y = split_data_sample(df_sub, 'Other')
            #For each model, train on the network            
			for model_name, model in models.items():
                # Save start training time
				start_time = time.time()
				print("Training Start time:", time.time())
				model.train(x=x, y=y)
                # Save end training time
				training_time = (time.time() - start_time)
				print("Training time in seconds: %.5f seconds" % training_time)
				training_time_models[model_name] = str(training_time)
		saver.save(sess, save_path="./temp/")
		mean_losses = {}
		outputs = {}
		# Save start testing time
		start_time = time.time()
		print("Testing Start time:", start_time)
		#Run the testing experiments 100 times        
		for i in np.arange(n_tests):
			print("Testing with Task #{}".format(i + 1))
			if i == 0:
				dates_list_train, sites_list_train = save_dates_sites(df_train)
				dates_list, sites_list = save_dates_sites(df_test)
				remove_date_site_columns(df_train)
				remove_date_site_columns(df_test)
			column_names = []
			column_names.extend(df.columns)
			column_names.remove(output_column)
			column_names.remove('Date')
			column_names.remove('Site Id')
			column_names.remove('Climate')
			X_train, y = split_features_label(df_train)
			X_test, eval_y = split_features_label(df_test)
			scaler = MinMaxScaler()
			x = scaler.fit_transform(X_train)
			eval_x = scaler.transform(X_test)
			average_output = np.mean(y)
			print("average for output is", average_output)  
			for model_name, model in models.items():
				if model_name not in mean_losses:
					mean_losses[model_name] = []
				model_loss, model_output = model.test(x=x, y=y, test_x=eval_x, test_y=eval_y)
				mean_losses[model_name].append(model_loss)
				outputs[model_name] = model_output[-1]
				#Save training and testing data sets for each model for the last test iteration
				if i == n_tests - 1 :
					# Export testing data
					X_test_df = pd.DataFrame(eval_x, columns=column_names)
					predicted_df = pd.DataFrame(np.array(outputs[model_name]))
					date_df = pd.DataFrame(np.array(dates_list))
					sites_df = pd.DataFrame(np.array(sites_list))
					X_test_df['Date'] = date_df
					X_test_df["Site Id"] = sites_df
					X_test_df[output_column] = eval_y
					X_test_df[output_column + "_predicted"] = predicted_df
					X_test_df.to_csv(output_path + model_name + '_test_dataset.csv')

					# Export training data
					X_train_df = pd.DataFrame(x, columns=column_names)
					date_df = pd.DataFrame(np.array(dates_list_train))
					sites_df = pd.DataFrame(np.array(sites_list_train))
					X_train_df['Date'] = date_df
					X_train_df["Site Id"] = sites_df
					X_train_df[output_column] = y
					X_train_df.to_csv(output_path + model_name + '_train_dataset.csv')
		for model_name, mean_loss in mean_losses.items():
			mean_losses[model_name] = np.mean(mean_loss, axis=0)
			# Save end testing time
			testing_time = (time.time() - start_time)
			print("Testing time in seconds: %.5f seconds" % testing_time)
			testing_time_models[model_name] = str(testing_time)

		# Plot graident steps versus loss for each model
		fig, ax = plt.subplots()
		for model_name, mean_loss in mean_losses.items():
			ax.plot(np.arange(grad_steps), mean_loss, label=model_name)
		
		ax.legend()
		fig.suptitle('Gradients steps versus MSE', fontsize=20)
		plt.xlabel('Gradient steps', fontsize=18)
		plt.ylabel('Loss', fontsize=16)
		fig.savefig(output_path + 'gradient_loss_' + str(grad_steps))
		plt.close()

		for model_name, model_output in outputs.items():
			# Compute test error metrics and export results
			metrics = dataPreparation.error_metrics(eval_y, model_output, average_output, len(eval_y), columns)
			print(metrics)
			rel_points = np.array([
							        [1, 0 , 0],
							        [4, 0 , 0],
							        [15, 1 , 0]
    							 ])
			metrics = dataPreparation.evaluate(average_output, len(eval_y), columns, eval_y, model_output, 1, 0.1, "range", "high", 1.5, rel_points)
			metrics["Training Time (seconds)"] = training_time_models[model_name]
			metrics["Testing Time (seconds)"] = testing_time_models[model_name]
			print("metrics for model:", model_name, metrics)
			dataPreparation.export_scores(testing_path, metrics, 'Testing ' + str(model_name))

			# Plot scatter plot and actual versus predicted for output for each model
			dataPreparation.plot_actual_vs_predicted(eval_y, model_output, model_name)
			dataPreparation.plot_actual_vs_predicted_scatter_bisector(eval_y, model_output, model_name)
		
		
