from datasets import DatasetTrain
from model import Network

import torch
import torch.utils.data
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F

import numpy as np
import pickle
import time
import os
import json
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from dataset_preparation import DataPreparation

dataPreparation = DataPreparation()
path = dataPreparation.output_path
experiment_name = dataPreparation.experiment_name

# Hyper parameters
num_epochs = 300
batch_size = 32
learning_rate = 0.001
alpha = 1.0
p = 0.2
M = 10

model_id = experiment_name + "_M" + str(M)

# Save parameters
parameters = {}
parameters['number of epochs'] = num_epochs
parameters['train split'] = 0.8
parameters['learning rate'] = learning_rate
parameters['hidden neurons'] = 64
parameters['activation'] = 'Relu'
parameters['dropout'] = p
parameters['number of ensembles'] = M
parameters['number of layers'] = 2
parameters['optimizer'] = 'Adam'
parameters['batch size'] = batch_size
dataPreparation.write_dict_to_json(path + "parameters", parameters)

train_dataset = DatasetTrain()
N = float(len(train_dataset))
print (N)

num_train_batches = int(len(train_dataset)/batch_size)
print ("num_train_batches:", num_train_batches)

train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)

# Save start training time
start_time = time.time()
print("Training Start time:", time.time())

for i in range(M):
    network = Network(model_id + "_%d" % i, project_dir=path)

    optimizer = torch.optim.Adam(network.parameters(), lr=learning_rate)

    epoch_losses_train = []
    for epoch in range(num_epochs):
        print ("###########################")
        print ("######## NEW EPOCH ########")
        print ("###########################")
        print ("epoch: %d/%d" % (epoch+1, num_epochs))
        print ("network: %d/%d" % (i+1, M))

        network.train() # (set in training mode, this affects BatchNorm and dropout)
        batch_losses = []
        for step, (x, y) in enumerate(train_loader):
            x = Variable(x).unsqueeze(1) # (shape: (batch_size, 1))
            y = Variable(y).unsqueeze(1) # (shape: (batch_size, 1))

            outputs = network(x)
            mean = outputs[0] # (shape: (batch_size, ))
            log_var = outputs[1] # (shape: (batch_size, )) (log(sigma^2))

            ####################################################################
            # compute the loss:
            ####################################################################
            loss_likelihood = torch.mean(torch.exp(-log_var)*torch.pow(y - mean, 2) + log_var)

            loss_prior = 0.0
            for param in network.parameters():
                if param.requires_grad:
                    loss_prior += ((1.0 - p)/N)*(1.0/alpha)*torch.sum(torch.pow(param, 2))

            loss = loss_likelihood + loss_prior

            loss_value = loss.data.cpu().numpy()
            batch_losses.append(loss_value)

            ########################################################################
            # optimization step:
            ########################################################################
            optimizer.zero_grad() # (reset gradients)
            loss.backward() # (compute gradients)
            optimizer.step() # (perform optimization step)

        epoch_loss = np.mean(batch_losses)
        epoch_losses_train.append(epoch_loss)
        with open("%s/epoch_losses_train.pkl" % network.model_dir, "wb") as file:
            pickle.dump(epoch_losses_train, file)
        print ("train loss: %g" % epoch_loss)
        plt.figure(1)
        plt.plot(epoch_losses_train, "k^")
        plt.plot(epoch_losses_train, "k")
        plt.ylabel("loss")
        plt.xlabel("epoch")
        plt.title("train loss per epoch")
        plt.savefig("%s/epoch_losses_train.png" % network.model_dir)
        plt.close(1)

        # save the model weights to disk:
        checkpoint_path = network.checkpoints_dir + "/model_" + model_id +"_epoch_" + str(epoch+1) + ".pth"
        torch.save(network.state_dict(), checkpoint_path)

# Save end training time
training_time = (time.time() - start_time)
print("Training time in seconds: %.5f seconds" % training_time)
dataPreparation.training_time = training_time