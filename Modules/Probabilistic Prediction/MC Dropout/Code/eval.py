from datasets import DatasetEval
from model import Network

import torch
import torch.utils.data
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F

import numpy as np
import os
import json
import time
import pickle
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from dataset_preparation import DataPreparation


batch_size = 32
M = 10
max_logvar = 2.0

dataPreparation = DataPreparation()
path = dataPreparation.output_path
experiment_name = dataPreparation.experiment_name
model_id = experiment_name + "_M" + str(M)

network = Network("eval_" + model_id, project_dir=path)
network.load_state_dict(torch.load(path + "training_logs/model_MC-Dropout-MAP-02-Adam_M10_%d/checkpoints/model_MC-Dropout-MAP-02-Adam_M10_epoch_300.pth" % 0))

M_float = float(M)
print (M_float)

val_dataset = DatasetEval()

num_val_batches = int(len(val_dataset)/batch_size)
print ("num_val_batches:", num_val_batches)

val_loader = torch.utils.data.DataLoader(dataset=val_dataset, batch_size=batch_size, shuffle=False)
df = val_dataset.df
X_test_exp = val_dataset.X_test_exp

# Save start testing time
start_time = time.time()
print("Testing Start time:", start_time)

network.eval()
x_values = []
y_values = []
final_mean_values = []
final_sigma_tot_values = []
final_sigma_epi_values = []
final_sigma_alea_values = []
for step, (x, y) in enumerate(val_loader):
    x = Variable(x).unsqueeze(1) # (shape: (batch_size, 1))
    y = Variable(y).unsqueeze(1) # (shape: (batch_size, 1))

    means = []
    vars = []
    for i in range(M):
        outputs = network(x)
        mean = outputs[0] # (shape: (batch_size, ))
        var = outputs[1] # (shape: (batch_size, )) (log(sigma^2))
        var = max_logvar - F.relu(max_logvar-var)

        means.append(mean)
        vars.append(var)

    for i in range(x.size(0)):
        x_value = x[i].data.cpu().numpy()[0]
        y_value = y[i].data.cpu().numpy()[0]

        mean_values = []
        for mean in means:
            mean_value = mean[i].data.cpu().numpy()[0]
            mean_values.append(mean_value)

        sigma_alea_values = []
        for var in vars:
            sigma_alea_value = torch.exp(var[i]).data.cpu().numpy()[0]
            sigma_alea_values.append(sigma_alea_value)

        mean_value = 0.0
        for value in mean_values:
            mean_value += value/M_float

        sigma_epi_value = 0.0
        for value in mean_values:
            sigma_epi_value += ((mean_value - value)**2)/M_float

        sigma_alea_value = 0.0
        for value in sigma_alea_values:
            sigma_alea_value += value/M_float

        sigma_tot_value = sigma_epi_value + sigma_alea_value

        x_values.append(x_value)
        y_values.append(y_value)
        final_mean_values.append(mean_value)
        final_sigma_epi_values.append(sigma_epi_value)
        final_sigma_alea_values.append(sigma_alea_value)
        final_sigma_tot_values.append(sigma_tot_value)


# Save end testing time
testing_time = (time.time() - start_time)
print("Testing time in seconds: %.5f seconds" % testing_time)

# Remove un-used columns
columns = []
columns.extend(df.columns)
columns.remove("Site Id")
columns.remove("Date")
columns.remove(dataPreparation.output_column)
print("Columns", columns, len(columns))
num_columns = len(columns)
num_rows = df.shape[0]
average_output = np.mean(df['LE_bowen_corr(mm)'])

df_test = dataPreparation.export_test_dataset(X_test_exp, final_mean_values, [final_sigma_alea_values, final_sigma_epi_values, final_sigma_tot_values], ['Aleatoric Uncertainty', 'Epistemic Uncertainty', 'Total Uncertainty'])


# Calculate error metrics
rel_points = np.array([[1, 0 , 0],[4, 0 , 0],[15, 1 , 0]])
test_metrics = dataPreparation.evaluate(average_output, num_rows, num_columns, np.array(y_values), np.array(final_mean_values), 1, 0.1, "range", "high", 1.5, rel_points)
# test_metrics = dataPreparation.error_metrics(np.array(y_values), np.array(final_mean_values), average_output, num_rows, num_columns, "")
test_metrics["Training Time (seconds)"] = 0
test_metrics["Testing Time (seconds)"] = testing_time
print("test metrics", test_metrics)

# Export metrics
dataPreparation.export_scores(path + "test_scores.csv", test_metrics, "Testing MCDropout")

# Plot training and test
file_name = "mcdropout_" + str(M)
dataPreparation.plot_actual_vs_predicted(y_values, final_mean_values, file_name)
dataPreparation.plot_actual_vs_predicted_scatter_bisector(np.array(y_values), np.array(final_mean_values), file_name)
plt.close()


def plot_mean_vs_truth_with_uncertainties(x_truth, y_truth, x_prediction, y_prediction,
                                          aleatoric, epistemic, ax=None, num_samples=100):
    """
    Same as plot_mean_vs_truth but with the uncertainties splitted into aleatoric and epistemic.

    :param x_truth:
    :param y_truth:
    :param x_prediction:
    :param y_prediction:
    :param std:
    :return: fig, ax
    """
    if not ax:
        ax = plt.gca()

    x_prediction_sample = x_prediction[:num_samples, 0]
    y_truth_sample = y_truth[:num_samples]
    y_prediction_sample = y_prediction[:num_samples]
    aleatoric_sample = aleatoric[:num_samples]
    epistemic_sample = epistemic[:num_samples]

    ax.scatter(x_truth[:num_samples, 0], y_truth_sample, label="Truth", color="blue")
    ax.plot(x_prediction_sample, y_prediction_sample, label="Prediction", color="red")

    # inner tube
    val1 = np.array(y_prediction_sample) - 2*np.sqrt(aleatoric_sample)
    val2 = np.array(y_prediction_sample) + 2*np.sqrt(aleatoric_sample)
    val3 = np.array(y_prediction_sample) - 2*np.sqrt(aleatoric_sample) - 2*np.sqrt(np.array(epistemic_sample))
    val4 = np.array(y_prediction_sample) + 2*np.sqrt(aleatoric_sample) + 2*np.sqrt(np.array(epistemic_sample))

    # val1 = np.array(y_prediction_sample) - aleatoric_sample
    # val2 = np.array(y_prediction_sample) + aleatoric_sample
    # val3 = np.array(y_prediction_sample) - aleatoric_sample - epistemic_sample / 2.0
    # val4 = np.array(y_prediction_sample) + aleatoric_sample + epistemic_sample / 2.0

    ax.fill_between(x_prediction_sample.flatten(), val1[0], val2[0],
                    color="green", alpha=0.2, label="aleatoric")

    # two outer tubes
    ax.fill_between(x_prediction_sample.flatten(), val1[0] , val3[0],
                    color="orange", alpha=0.3, label="epistemic")

    ax.fill_between(x_prediction_sample.flatten(), val2[0] ,val4[0],
                    color="orange", alpha=0.3)
    
    ax.legend()
    plt.ylabel("x")
    plt.xlabel("y")
    plt.title("Uncertainty Types")
    plt.savefig("%s/all_uncertainty.png" % network.model_dir)
    plt.close(1)


plot_mean_vs_truth_with_uncertainties(np.array(x_values), np.array(y_values), np.array(x_values),
 np.array(final_mean_values), np.array(final_sigma_alea_values), np.array(final_sigma_epi_values), ax=None, num_samples=50)

index = 10
plt_x = np.array(x_values)[:index, 0]
plt_y = np.array(y_values)[:index, 0]
plt_mean = np.array(final_mean_values)[:index, 0]
plt_sigma_alea = np.array(final_sigma_alea_values)[:index, 0]
plt_sigma_epis = np.array(final_sigma_epi_values)[:index, 0]
plt_sigma_tot = np.array(final_sigma_tot_values)[:index, 0]

plt.figure(1)
plt.plot(plt_x, plt_mean, "r", linestyle="",marker="x")
plt.fill_between(plt_x, plt_mean - 2*np.sqrt(plt_sigma_alea), np.array(plt_mean) + 2*np.sqrt(plt_sigma_alea), color="C3", alpha=0.25)
plt.plot(plt_x, plt_y, "k", linestyle="",marker="o")
plt.fill_between(plt_x, plt_y - 2*0.15*(1.0/(1 + np.exp(-plt_x))), plt_y + 2*0.15*(1.0/(1 + np.exp(-plt_x))), color="0.5", alpha=0.25)
plt.ylabel("mu(x)")
plt.xlabel("x")
plt.title("predicted vs true mean(x) with aleatoric uncertainty")
plt.savefig("%s/mu_alea_pred_true.png" % network.model_dir)
plt.close(1)


plt.figure(1)
plt.plot(plt_x, plt_y)
plt.fill_between(plt_x, plt_y - 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), plt_y + - 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), color="0.5", alpha=0.25)
plt.ylabel("mu(x)")
plt.xlabel("x")
plt.title("true mean(x) with aleatoric uncertainty")
plt.savefig("%s/mu_alea_true.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, plt_mean, "r")
plt.fill_between(plt_x, plt_mean - 2*np.sqrt(np.array(plt_sigma_alea)), plt_mean + 2*np.sqrt(np.array(plt_sigma_alea)), color="C3", alpha=0.25)
plt.ylabel("mu(x)")
plt.xlabel("x")
plt.title("predicted mean(x) with aleatoric uncertainty")
plt.savefig("%s/mu_alea_pred.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, np.sqrt(np.array(plt_sigma_alea)), "r")
plt.plot(plt_x, 0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), "k")
plt.xlabel("x")
plt.title("predicted vs true aleatoric uncertainty")
plt.savefig("%s/alea_pred_true.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, np.sqrt(np.array(plt_sigma_epis)), "r")
plt.xlabel("x")
plt.title("predicted epistemic uncertainty")
plt.savefig("%s/epi_pred.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, plt_mean, "r")
plt.fill_between(plt_x, np.array(plt_mean) - 2*np.sqrt(np.array(plt_sigma_epis)), np.array(plt_mean) + 2*np.sqrt(np.array(plt_sigma_epis)), color="C1", alpha=0.25)
plt.plot(plt_x, plt_y, "k")
plt.ylabel("mu(x)")
plt.xlabel("x")
plt.title("predicted vs true mean(x) with epistemic uncertainty")
plt.savefig("%s/mu_epi_pred_true.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, plt_mean, "r")
plt.fill_between(plt_x, np.array(plt_mean) - 2*np.sqrt(np.array(plt_sigma_tot)), np.array(plt_mean) + 2*np.sqrt(np.array(plt_sigma_tot)), color="C2", alpha=0.25)
plt.plot(plt_x, plt_y, "k")
plt.fill_between(plt_x, plt_y - 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), plt_y + 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), color="0.5", alpha=0.25)
plt.ylabel("mu(x)")
plt.xlabel("x")
plt.title("predicted vs true mean(x) with total uncertainty")
plt.savefig("%s/mu_tot_pred_true.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, plt_y, "k")
plt.fill_between(plt_x, plt_y - 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), plt_y + 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), color="0.5", alpha=0.25)
plt.tight_layout(pad=0.1, w_pad=0.1, h_pad=0.1)
plt.savefig("%s/predictive_density_GT_.png" % network.model_dir)
plt.close(1)

plt.figure(1)
plt.plot(plt_x, plt_y, "r")
plt.fill_between(plt_x, np.array(plt_mean) - 2*np.sqrt(np.array(plt_sigma_tot)), np.array(plt_mean) + 2*np.sqrt(np.array(plt_sigma_tot)), color="C3", alpha=0.25)
plt.plot(plt_x, plt_y, "k")
plt.fill_between(plt_x, plt_y - 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), plt_y + 2*0.15*(1.0/(1 + np.exp(-np.array(plt_x)))), color="0.5", alpha=0.25)
plt.tight_layout(pad=0.1, w_pad=0.1, h_pad=0.1)
plt.savefig("%s/predictive_density_.png" % network.model_dir)
plt.close(1)
