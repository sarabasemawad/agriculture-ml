import torch
import torch.utils.data
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import pandas as pd

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import pickle
from dataset_preparation import DataPreparation

x_test, y_test, x_test_exp = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
dataPreparation = DataPreparation()
df = dataPreparation.read_data()
print("columns after read are", df.columns)
X_train, Y_train, X_test, Y_test, X_valid, Y_valid, X_test_exp  = dataPreparation.process_data(df, scaleMinMax = True)
# .process_train_test_data(df, scaleMinMax = True)
print("X_train shape", X_train.shape, "X_test shape", X_test.shape, "X valid shape", X_valid.shape)
output_path = dataPreparation.output_path

class DatasetTrain(torch.utils.data.Dataset):
    def __init__(self):
        self.examples = []

        plt.figure(1)
        plt.plot(X_train, Y_train, "k.")
        plt.ylabel("y")
        plt.xlabel("x")
        plt.savefig(output_path + "training_data.png")
        plt.close(1)

        for i in range(X_train.shape[0]):
            example = {}
            example["x"] = X_train[i].astype(np.float32)
            example["y"] = np.array(Y_train)[i].astype(np.float32)
            self.examples.append(example)

        self.num_examples = len(self.examples)

    def __getitem__(self, index):
        example = self.examples[index]

        x = example["x"]
        y = example["y"]

        return (x, y)

    def __len__(self):
        return self.num_examples
    
class DatasetEval(torch.utils.data.Dataset):
    df = df
    X_test_exp = X_test_exp
    def __init__(self):
        self.examples = []
        print("validation shapes:", X_valid.shape, Y_valid.shape)
        for i in range(X_valid.shape[0]):
            example = {}
            example["x_test"] = X_valid[i].astype(np.float32)
            example["y_test"] = np.array(Y_valid)[i].astype(np.float32)
            self.examples.append(example)
        self.num_examples = len(self.examples)
        print("num examples", len(self.examples))

    def __getitem__(self, index):
        example = self.examples[index]

        x = example["x_test"]
        y = example["y_test"]
        return (x, y)

    def __len__(self):
        return self.num_examples

class DatasetTest(torch.utils.data.Dataset):
    df = df
    X_test_exp = X_test_exp
    def __init__(self):
        self.examples = []
        print("test shapes:", X_test.shape, Y_test.shape)
        for i in range(X_test.shape[0]):
            example = {}
            example["x_test"] = X_test[i].astype(np.float32)
            example["y_test"] = np.array(Y_test)[i].astype(np.float32)
            self.examples.append(example)
        self.num_examples = len(self.examples)
        print("num examples", len(self.examples))

    def __getitem__(self, index):
        example = self.examples[index]

        x = example["x_test"]
        y = example["y_test"]
        return (x, y)

    def __len__(self):
        return self.num_examples
